<?php

namespace App\Http\Controllers\Admin\BrandLogo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Image;


class BrandLogoController extends Controller
{
    public function BrandLogo()
    {
    	$brand = DB::table('brand_logos')->where('status',1)->get();

    	return view('admin.BrandLogo.brand',compact('brand'));


    }

    public function StoreBrandLogo(REQUEST $request)
    {
    	$data = array();
    	$data['tag_line'] = $request->tagline;
    	$data['status'] = 1;

    	$brand_logo = $request->brand;

    	if($brand_logo){
	    	$logo= hexdec(uniqid()).'.'.$brand_logo->getClientOriginalExtension();
	        Image::make($brand_logo)->resize(60,60)->save('public/assets/AdminBackend/BrandLogo/'.$logo);                    
	        $data['brand_logo']='public/assets/AdminBackend/BrandLogo/'.$logo;

	        $added = DB::table('brand_logos')->insert($data);

	        if($added) {
	        	$notification=array(
	             'messege'=>'Successfully Added!',
	             'alert-type'=>'success'
	              );
	        	return Redirect()->route('brand')->with($notification);
	        }else{
	        	$notification=array(
	             'messege'=>'Something going to wrong!',
	             'alert-type'=>'error'
	              );
	        	return Redirect()->route('brand')->with($notification);
	        }    

	        
		}else{
			$notification=array(
             'messege'=>'Please Select Brand Logo!',
             'alert-type'=>'error'
              );
        return Redirect()->route('brand')->with($notification);
		}

    }


    public function DeleteBrandLogo($id)
    {
    	$brandData = DB::table('brand_logos')->where('id',$id)->first();
    	$oldLogo = $brandData->brand_logo;

    	$delete = DB::table('brand_logos')->where('id',$id)->delete();

    	if($delete) {
    		unlink($oldLogo);
	        	$notification=array(
	             'messege'=>'Delete Done!',
	             'alert-type'=>'success'
	              );
	        	return Redirect()->route('brand')->with($notification);
	        }else{
	        	$notification=array(
	             'messege'=>'Try Again!',
	             'alert-type'=>'error'
	              );
	        	return Redirect()->route('brand')->with($notification);
	        }
    }
}
