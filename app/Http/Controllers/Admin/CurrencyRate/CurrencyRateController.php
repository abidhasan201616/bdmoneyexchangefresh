<?php

namespace App\Http\Controllers\Admin\CurrencyRate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Image;

class CurrencyRateController extends Controller
{
    public function ViewCurrencyRate()
   	{
   		$currencyRate = DB::table('currency_rates')->get();
   		return view('admin.CurrencyRate.currency_rate',compact('currencyRate'));
   	}

   	public function AddCurrencyRate(REQUEST $request)
   	{
   		$data = array();
   		$data['we_buy'] = $request->buy_rate;
   		$data['buy_currency_name'] = $request->buy_currency_name;
   		$data['we_sell'] = $request->sell_rate;
   		$data['sell_currency_name'] = $request->sell_currency_name;
   		$data['status'] = 1;

		$buy_currency_sign = $request->buy_currency_sign;
		$sell_currency_sign = $request->sell_currency_sign;

    	if($buy_currency_sign && $sell_currency_sign){
	    	$buy_currency= hexdec(uniqid()).'.'.$buy_currency_sign->getClientOriginalExtension();
	        Image::make($buy_currency_sign)->resize(30,30)->save('public/assets/AdminBackend/Currency_Sign/'.$buy_currency); 

	        $data['buy_currency_sign'] ='public/assets/AdminBackend/Currency_Sign/'.$buy_currency;


	        $sell_currency= hexdec(uniqid()).'.'.$sell_currency_sign->getClientOriginalExtension();
	        Image::make($sell_currency_sign)->resize(30,30)->save('public/assets/AdminBackend/Currency_Sign/'.$sell_currency); 

	        $data['sell_currency_sign'] ='public/assets/AdminBackend/Currency_Sign/'.$sell_currency;

	        DB::table('currency_rates')->insert($data);
	        return redirect()->route('currency.rate');
    	}else{

    		echo "Not found Buy Currency Sign";
    		DB::table('currency_rates')->insert($data);
    		return redirect()->route('currency.rate');
    	}
   		
   		
   	}

   	public function EditCurrencyRate($id)
   	{

   		$currencyData = DB::table('currency_rates')->where('id',$id)->first();

   		return view('admin.CurrencyRate.edit',compact('currencyData'));
   	}

    public function UpdateCurrencyRate(REQUEST $request,$id)
    {
      $oldData = DB::table('currency_rates')->where('id',$id)->first();
      $old_buy_sign = $oldData->buy_currency_sign;
      $old_sell_sign = $oldData->sell_currency_sign;


      $data = array();
      $data['we_buy'] = $request->buy_rate;
      $data['buy_currency_name'] = $request->buy_currency_name;
      $data['we_sell'] = $request->sell_rate;
      $data['sell_currency_name'] = $request->sell_currency_name;
      $data['status'] = 1;


      $buy_currency_sign = $request->new_buy_currency_sign;
      $sell_currency_sign = $request->new_sell_currency_sign;


      if($buy_currency_sign && $sell_currency_sign){



        $buy_currency= hexdec(uniqid()).'.'.$buy_currency_sign->getClientOriginalExtension();
          Image::make($buy_currency_sign)->resize(30,30)->save('public/assets/AdminBackend/Currency_Sign/'.$buy_currency); 

          $data['buy_currency_sign'] ='public/assets/AdminBackend/Currency_Sign/'.$buy_currency;
          unlink($old_buy_sign);

          $sell_currency= hexdec(uniqid()).'.'.$sell_currency_sign->getClientOriginalExtension();
          Image::make($sell_currency_sign)->resize(30,30)->save('public/assets/AdminBackend/Currency_Sign/'.$sell_currency); 

          $data['sell_currency_sign'] ='public/assets/AdminBackend/Currency_Sign/'.$sell_currency;
          unlink($old_sell_sign);

          DB::table('currency_rates')->where('id',$id)->update($data);
          return redirect()->route('currency.rate');

      }else{

        echo "Not found Buy Currency Sign";
        DB::table('currency_rates')->where('id',$id)->update($data);
        return redirect()->route('currency.rate');
      }


    }

    public function DeactiveCurrencyRate($id)
    {

      $data = array();
      $data['status'] = 0;


     $deactive =  DB::table('currency_rates')->where('id',$id)->update($data);

     if($deactive){

      $notification=array(
             'messege'=>'Deactive Successfully!',
             'alert-type'=>'success'
              );
        return Redirect()->route('currency.rate')->with($notification);

      }else{
        $notification=array(
             'messege'=>'Deactive Unsuccessfull!',
             'alert-type'=>'error'
              );
        return Redirect()->route('currency.rate')->with($notification);
      }

       
    }

// active Start
    public function ActiveCurrencyRate($id)
    {
      $data = array();
      $data['status'] = 1;


     $active =  DB::table('currency_rates')->where('id',$id)->update($data);

     if($active){

      $notification=array(
             'messege'=>'Active Successfully!',
             'alert-type'=>'success'
              );
        return Redirect()->route('currency.rate')->with($notification);

      }else{
        $notification=array(
             'messege'=>'Active Unsuccessfull!',
             'alert-type'=>'error'
              );
        return Redirect()->route('currency.rate')->with($notification);
      }



    } 
    // active end

 // Delete Start
    public function DeleteCurrencyRate($id)
    {
      $delete = DB::table('currency_rates')->where('id',$id)->delete();

      if($delete){

      $notification=array(
             'messege'=>'Delete Successfully!',
             'alert-type'=>'success'
              );
        return Redirect()->route('currency.rate')->with($notification);

      }else{
        $notification=array(
             'messege'=>'Delete Unsuccessfull!',
             'alert-type'=>'error'
              );
        return Redirect()->route('currency.rate')->with($notification);
      }
    }
     // Delete end



}
