<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class HeaderMainController extends Controller
{
    public function HeaderMenu()
    {
    	return view('Admin.AdminPages.header_main_menu');
    }

    public function AddMenu(REQUEST $request)
    {
    	
    	$data = array();
    	$data['menu_item'] = $request->menu_item;
        $data['status'] = 1;

    	// dd($data);

    	DB::table('header_menues')->insert($data);

    	return redirect()->back();
    }

    public function DeactiveStatus($id){

    	$data = array();
    	$data['status'] = 0;

    	DB::table('header_menues')->where('id',$id)->update($data);

    	return redirect()->back();

    }

    public function ActiveStatus($id){

            $data = array();
            $data['status'] = 1;

            DB::table('header_menues')->where('id',$id)->update($data);

            return redirect()->back();

    }


    public function EditMenuItem($id)
    {
    	$menu = DB::table('header_menues')->where('id',$id)->first();


    	return view('admin.Header_Menu.edit',compact('menu'));
    }

    public function UpdateMenuItem(REQUEST $request,$id)
    {
    	$data = array();
    	$data['menu_item'] = $request->menu_item;

    	// dd($data);

    	DB::table('header_menues')->where('id',$id)->update($data);

    	return redirect()->route('header.mainmenu');
    }

}
