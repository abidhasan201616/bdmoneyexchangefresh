<?php

namespace App\Http\Controllers\Admin\PaymentStatus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;


class PaymentStatusController extends Controller
{
    public function index()
    {
    	$PaymentStatus = DB::table('payment_status')->get();
    	return view('admin.payment.payment_status',compact('PaymentStatus'));
    }

    public function StorePaymentStatus(REQUEST $request)
    {
    	$data = array();
    	$data['status_name'] = $request->status_name;
    	$data['status'] = $request->status_number;

    	$success = DB::table('payment_status')->insert($data);

    	if($success){

	      $notification=array(
	             'messege'=>'Successfully Added!',
	             'alert-type'=>'success'
	              );
	        return Redirect()->route('payment.status')->with($notification);

	      }else{
	        $notification=array(
	             'messege'=>'Try Again!',
	             'alert-type'=>'error'
	              );
	        return Redirect()->route('payment.status')->with($notification);
	      }

    }

    public function EditPaymentStatus($id)
    {
    	$PaymentStatus = DB::table('payment_status')->where('id',$id)->first();
    	return view('admin.payment.payment_status_edit',compact('PaymentStatus'));
    }

    public function UpdatePaymentStatus(REQUEST $request,$id)
    {
    	$data = array();
    	$data['status_name'] = $request->status_name;
    	$data['status'] = $request->status_number;

    	$success = DB::table('payment_status')->where('id',$id)->update($data);

    	if($success){

	      $notification=array(
	             'messege'=>'Successfully Update!',
	             'alert-type'=>'success'
	              );
	        return Redirect()->route('payment.status')->with($notification);

	      }else{
	        $notification=array(
	             'messege'=>'Try Again!',
	             'alert-type'=>'error'
	              );
	        return Redirect()->route('payment.status')->with($notification);
	      }
    }

	public function DeletePaymentStatus($id)
	{
		$success = DB::table('payment_status')->where('id',$id)->delete();

		if($success){

	      $notification=array(
	             'messege'=>'Successfully Delete!',
	             'alert-type'=>'success'
	              );
	        return Redirect()->route('payment.status')->with($notification);

	      }else{
	        $notification=array(
	             'messege'=>'Try Again!',
	             'alert-type'=>'error'
	              );
	        return Redirect()->route('payment.status')->with($notification);
	      }

	}


}
