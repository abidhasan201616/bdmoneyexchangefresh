<?php

namespace App\Http\Controllers\Admin\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Image;

class ProfileController extends Controller
{
    public function Profile()
    {

    	error_reporting(0);
    	$oldInfo = DB::table('profiles')->where('user_id',Auth::user()->id)->first();
    	
    	if($oldInfo !== null){
    		$oldUser = $oldInfo->user_id;
    		$profile = DB::table('profiles')->where('user_id',$oldUser)->first();
    		return view('Profile.index',compact('profile'));
    	}

    	
    	// dd($oldInfo);
		return view('Profile.index');
    	

    	
    }

    public function StoreProfileData(REQUEST $request)
    {
		error_reporting(0);
    	$oldInfo = DB::table('profiles')->where('user_id',Auth::user()->id)->first();
    	$oldUserId = $oldInfo->user_id;
    	// dd($oldUserId);
		
    	if($oldUserId){
    		$notification=array(
	             'messege'=>'Already Added!',
	             'alert-type'=>'success'
	              );
	        return Redirect()->route('profile')->with($notification);
    	}else{

    		$data = array();
	    	$data['user_id'] = Auth::user()->id;
	    	$data['address_one'] = $request->address_one;
	    	$data['address_two'] = $request->address_two;
	    	$data['city'] = $request->city;
	    	$data['state'] = $request->state;
	    	$data['zip'] = $request->zip_code;
	    	$data['post_code'] = $request->post_code;
	    	$data['country'] = $request->country;
	    	$data['phone'] = $request->phone;

	    	$added = DB::table('profiles')->insert($data);

	    	if($added){

		      $notification=array(
		             'messege'=>'Successfully Added!',
		             'alert-type'=>'success'
		              );
		        return Redirect()->route('profile')->with($notification);

		      }else{
		        $notification=array(
		             'messege'=>'Try Again!',
		             'alert-type'=>'error'
		              );
		        return Redirect()->route('profile')->with($notification);
		      }
	    }


    }

    public function UpdateProfileData(REQUEST $request,$id)
    {
    	$data = array();
    	$data['user_id'] = Auth::user()->id;
    	$data['address_one'] = $request->address_one;
    	$data['address_two'] = $request->address_two;
    	$data['city'] = $request->city;
    	$data['state'] = $request->state;
    	$data['zip'] = $request->zip_code;
    	$data['post_code'] = $request->post_code;
    	$data['country'] = $request->country;
    	$data['phone'] = $request->phone;

    	$updated = DB::table('profiles')->where('user_id',Auth::user()->id)->update($data);

    	if($updated){

	      $notification=array(
	             'messege'=>'Successfully Updated!',
	             'alert-type'=>'success'
	              );
	        return Redirect()->route('profile')->with($notification);

	      }else{
	        $notification=array(
	             'messege'=>'Try Again!',
	             'alert-type'=>'error'
	              );
	        return Redirect()->route('profile')->with($notification);
	      }
    }

    public function UpdateProfilePhoto(REQUEST $request,$id)
    {
    	$oldData =Auth::user()->photo;
    	if($oldData){
    		$oldPhoto = $oldData;
    	}
    	

    	$data = array();
    	$profile_photo = $request->photo;
    	

    	if($profile_photo){
	    	$photo= hexdec(uniqid()).'.'.$profile_photo->getClientOriginalExtension();
	        Image::make($profile_photo)->resize(160,192)->save('public/assets/UserFrontend/ProfilePhoto/'.$photo);                    
	        $data['photo']='public/assets/UserFrontend/ProfilePhoto/'.$photo;

	        $update = DB::table('users')->where('id',$id)->update($data);

	        if($oldData !== null){
		        unlink($oldPhoto);
		    }

	        if($update) {
	        	$notification=array(
	             'messege'=>'Successfully Upload!',
	             'alert-type'=>'success'
	              );
	        	return Redirect()->route('profile')->with($notification);
	        }else{
	        	$notification=array(
	             'messege'=>'Something going to wrong!',
	             'alert-type'=>'error'
	              );
	        	return Redirect()->route('profile')->with($notification);
	        }    

	        
		}else{
			$notification=array(
             'messege'=>'Please Select Profile Photo!',
             'alert-type'=>'error'
              );
        return Redirect()->route('profile')->with($notification);
		}


    }


}
