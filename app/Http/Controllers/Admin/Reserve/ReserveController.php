<?php

namespace App\Http\Controllers\Admin\Reserve;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
// use Intervention\Image\ImageManagerStatic as Image;
// use Intervention\Image\ImageManager;
use Image;

class ReserveController extends Controller
{
    public function ReserveAmount()
    {
    	$reserve = DB::table('our_reserveds')->get();

    	return view('admin.Reserve.reserve',compact('reserve'));
    }

    public function AddReserveAmount(REQUEST $request)
    {
    	$data = array();

    	$data['method_name'] = $request->method;
    	$data['reserve_amount'] = $request->reserve_amount;
        $data['currency_name'] = $request->currency_name;
        $data['status'] = 1;

    	$currency_sign = $request->currency_sign;

    	if($currency_sign){
	    	$currency= hexdec(uniqid()).'.'.$currency_sign->getClientOriginalExtension();
	        Image::make($currency_sign)->resize(30,30)->save('public/assets/AdminBackend/Currency_Sign/'.$currency);                    
	        $data['logo']='public/assets/AdminBackend/Currency_Sign/'.$currency;

	        DB::table('our_reserveds')->insert($data);
	     

	        return redirect()->route('reserve.amount');

    	}else{
    		
    		DB::table('our_reserveds')->insert($data);
    		 
    		return redirect()->route('reserve.amount');
    	}
    }

    public function EditReserveAmount($id)
    {
        $reserve = DB::table('our_reserveds')->where('id',$id)->first();

        return view('admin.Reserve.edit',compact('reserve'));
    }

    public function UpdateReserveAmount(REQUEST $request,$id)
    {
        $data = array();
        $data['method_name'] = $request->method;
        $data['reserve_amount'] = $request->reserve_amount;
        

        $reserve = DB::table('our_reserveds')->where('id',$id)->first();
        $old_currency_sign = $reserve->logo;        
        
        $currency_sign = $request->new_currency_sign;
        // dd($currency_sign);


        if($currency_sign){
            unlink($old_currency_sign);
            $currency= hexdec(uniqid()).'.'.$currency_sign->getClientOriginalExtension();
            Image::make($currency_sign)->resize(30,30)->save('public/assets/AdminBackend/Currency_Sign/'.$currency);                    
            $data['logo']='public/assets/AdminBackend/Currency_Sign/'.$currency;

            DB::table('our_reserveds')->where('id',$id)->update($data);
         

            return redirect()->route('reserve.amount');

        }else{
            
            DB::table('our_reserveds')->where('id',$id)->update($data);
             
            return redirect()->route('reserve.amount');
        }
    }


    public function ActiveReserveAmount($id)
    {
        $data = array();
        $data['status'] = 1;

        DB::table('our_reserveds')->where('id',$id)->update($data);

        return redirect()->route('reserve.amount');
    }

    public function DeactiveReserveAmount($id)
    {
        $data = array();
        $data['status'] = 0;

        DB::table('our_reserveds')->where('id',$id)->update($data);

        return redirect()->route('reserve.amount');
    }

    public function DeleteReserveAmount($id)
    {
        DB::table('our_reserveds')->where('id',$id)->delete();
        return redirect()->route('reserve.amount');
    }

}
