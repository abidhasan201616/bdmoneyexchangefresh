<?php

namespace App\Http\Controllers\Admin\Review;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;

class ReviewController extends Controller
{
    public function review()
    {

    	$review = DB::table('reviews')->get();
    	return view('admin.review.review',compact('review'));
    }

    public function StoreReview(REQUEST $request)
    {

    	if(Auth::check())
    	{
    		$data = array();
	    	$data['name'] = $request->name;
	    	$data['review'] = $request->review;
	    	$data['status'] = 0;

	    	$review = DB::table('reviews')->insert($data);

	    	 if($review){

		      $notification=array(
		             'messege'=>'Successfully added & Pending!',
		             'alert-type'=>'success'
		              );
		        return redirect()->route('home')->with($notification);

		      }else{
		        $notification=array(
		             'messege'=>'Try Again!',
		             'alert-type'=>'error'
		              );
		        return redirect()->route('home')->with($notification);
		      }
		  }else{
		  	$notification=array(
	             'messege'=>'Login First!',
	             'alert-type'=>'error'
	              );
	        return redirect()->route('login')->with($notification);
		  }
    	

    }

    public function AprooveReview($id)
    {
    	$data = array();
    	$data['status'] = 1;

    	$aprooved = DB::table('reviews')->where('id',$id)->update($data);

    	if($aprooved){

	      $notification=array(
	             'messege'=>'Successfully Aprooved!',
	             'alert-type'=>'success'
	              );
	        return redirect()->route('review')->with($notification);

	      }else{
	        $notification=array(
	             'messege'=>'Try Again!',
	             'alert-type'=>'error'
	              );
	        return redirect()->route('review')->with($notification);
	      }
    }

    public function PendingReview($id)
    {
    	$data = array();
    	$data['status'] = 0;

    	$pending = DB::table('reviews')->where('id',$id)->update($data);

    	if($pending){

	      $notification=array(
	             'messege'=>'Added Pending list!',
	             'alert-type'=>'success'
	              );
	        return redirect()->route('review')->with($notification);

	      }else{
	        $notification=array(
	             'messege'=>'Try Again!',
	             'alert-type'=>'error'
	              );
	        return redirect()->route('review')->with($notification);
	      }


    }

    public function DeleteReview($id)
    {
    	$delete = DB::table('reviews')->where('id',$id)->delete();

    	if($delete){

	      $notification=array(
	             'messege'=>'Successfully Delete!',
	             'alert-type'=>'success'
	              );
	        return redirect()->route('review')->with($notification);

	      }else{
	        $notification=array(
	             'messege'=>'Try Again!',
	             'alert-type'=>'error'
	              );
	        return redirect()->route('review')->with($notification);
	      }

    }



}
