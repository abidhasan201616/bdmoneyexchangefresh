<?php

namespace App\Http\Controllers\Admin\SendReceiveMethod;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class SendReceiveMethodController extends Controller
{
    public function SendReceiveView()
    {
    	$sendMethodInfo = DB::table('sends')->get();
    	$receiveMethodInfo = DB::table('receives_methods')->get();
    	return view('admin.SendReceiveMethod.send_receive',compact('sendMethodInfo','receiveMethodInfo'));
    }

    public function AddSendMethod(REQUEST $request)
    {
    	$data = array();
    	$data['send_method_name'] = $request->send_method_name;
    	$data['status'] = 1;

    	$success = DB::table('sends')->insert($data);

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Added!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }

    public function EditSendMethod($id)
    {
    	$sendMethodInfo = DB::table('sends')->where('id',$id)->first();
    	return view('admin.SendReceiveMethod.EditsendMethod',compact('sendMethodInfo'));
    }

    public function UpdateSendMethod(REQUEST $request,$id)
    {
    	$data = array();
    	$data['send_method_name'] = $request->send_method_name;
    	// $data['status'] = 1;

    	$success = DB::table('sends')->where('id',$id)->update($data);

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Update!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }

    public function DeleteSendMethod($id)
    {
    	$success = DB::table('sends')->where('id',$id)->delete();

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Delete!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }

    public function AddReceiveMethod(REQUEST $request)
    {
    	$data = array();
    	$data['receive_method_name'] = $request->receive_method_name;
    	$data['status'] = 1;

    	$success = DB::table('receives_methods')->insert($data);

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Added!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }

    public function EditReceiveMethod($id)
    {
    	$receiveMethodInfo = DB::table('receives_methods')->where('id',$id)->first();
    	return view('admin.SendReceiveMethod.EditReceiveMethod',compact('receiveMethodInfo'));

    }

    public function UpdateReceiveMethod(REQUEST $request,$id)
    {
    	$data = array();
    	$data['receive_method_name'] = $request->receive_method_name;
    	$data['status'] = 1;

    	$success = DB::table('receives_methods')->where('id',$id)->update($data);

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Update!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }

    public function DeleteReceiveMethod($id)
    {
    	$success = DB::table('receives_methods')->where('id',$id)->delete();

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Delete!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }

    public function DeactiveReceiveMethod($id)
    {
    	$data = array();
    	$data['status'] = 0;

    	$success = DB::table('receives_methods')->where('id',$id)->update($data);

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Deactive!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }
    public function ActiveReceiveMethod($id)
    {
    	$data = array();
    	$data['status'] = 1;

    	$success = DB::table('receives_methods')->where('id',$id)->update($data);

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Active!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }

    public function DeactiveSendMethod($id)
    {
    	$data = array();
    	$data['status'] = 0;

    	$success = DB::table('sends')->where('id',$id)->update($data);

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Deactive!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }

    public function ActiveSendMethod($id)
    {
    	$data = array();
    	$data['status'] = 1;

    	$success = DB::table('sends')->where('id',$id)->update($data);

    	if($success){

	      $notification=array(
             'messege'=>'Successfully Active!',
             'alert-type'=>'success'
              );
	        return Redirect()->route('send.receive')->with($notification);

	      }else{
	        $notification=array(
             'messege'=>'Try Again!',
             'alert-type'=>'error'
              );
	        return Redirect()->route('send.receive')->with($notification);
	     }
    }

}
