<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;

class TransactionController extends Controller
{
    
	public function StepOne(REQUEST $request)
	{

		if(Auth::check()){
			$data = array();
			$data['send_method']= $request->send_method_name;
			$data['receive_method']= $request->receive_method_name;
			$data['send_amount']= $request->sent_amount;
			$data['receive_amount']= $request->receive_amount;

			// dd($data);

			$reserve = DB::table('our_reserveds')->get();
			$currencyRate = DB::table('currency_rates')->get();
			$review = DB::table('reviews')->get();

			$authId = Auth::user()->id;

			$profileData = DB::table('profiles')->where('user_id',$authId)->first();

			if($profileData){
				$userData = DB::table('users')
			->join('profiles','users.id','profiles.user_id')
			->select('users.name','users.email','profiles.*')
			->where('profiles.user_id',$authId)
			->get();

			// dd($userData);

			return view('UserHomeContent.transaction_form',compact('data','reserve','currencyRate','review','userData'));
			}else{
				$notification=array(
	             'messege'=>'First Update your profile!',
	             'alert-type'=>'error'
	              );
	        return Redirect()->back()->with($notification);
			}

			
		}else{
			$notification=array(
	             'messege'=>'Login First!',
	             'alert-type'=>'error'
	              );
	        return Redirect()->route('login')->with($notification);
		}
		
	}

	public function StoreTransaction(REQUEST $request)
	{

		$authId = Auth::user()->id;

		$data = array();
		$data['user_id'] = $authId;
		$data['order_id'] = $request->order_id;
		$data['name'] = $request->name;
		$data['email'] = $request->email;
		$data['phone'] = $request->phone;
		$data['address_one'] = $request->address_one;
		$data['address_two'] = $request->address_two;
		$data['sender_account'] = $request->sender_account;
		$data['txdId'] = $request->txdId;
		$data['receiver_account'] = $request->receiver_account;
		$data['send_method'] = $request->send_method;
		$data['receive_method'] = $request->receive_method;
		$data['send_amount'] = $request->send_amount;
		$data['receive_amount'] = $request->receive_amount;
		$data['status'] = 1;

		// dd($data);

		$success = DB::table('transactions')->insert($data);

		if($success){

	      $notification=array(
	             'messege'=>'Successfully Receive transactions!',
	             'alert-type'=>'success'
	              );
	        return Redirect()->route('transaction.success')->with($notification);

	      }else{
	        $notification=array(
	             'messege'=>'Try Again!',
	             'alert-type'=>'error'
	              );
	        return Redirect()->back()->with($notification);
	      }

	}

	public function SuccessTransaction()
	{
		return view('UserHomeContent.success_msg');
	}


	public function AdminViewTransaction()
	{

		$transactions = DB::table('transactions')->get();
		
		return view('admin.Transactions.transactions',compact('transactions'));
	}

	public function UpdateTransactionStatus(REQUEST $request,$id)
	{
		$data = array();

		$status = $request->transaction_status;

		if($status == 'pending'){
			$data['status'] = 1;

			$success = DB::table('transactions')->where('id',$id)->update($data);

			if($success){

		      $notification=array(
		             'messege'=>'Successfully Update!',
		             'alert-type'=>'success'
		              );
		        return redirect()->route('admin.view.transaction')->with($notification);

		      }else{
		        $notification=array(
		             'messege'=>'Try Again!',
		             'alert-type'=>'error'
		              );
		        return redirect()->route('admin.view.transaction')->with($notification);
		      }
		}
		elseif($status == 'processing')
		{
			$data['status'] = 2;

			$success = DB::table('transactions')->where('id',$id)->update($data);

			if($success){

		      $notification=array(
		             'messege'=>'Successfully Update!',
		             'alert-type'=>'success'
		              );
		        return redirect()->route('admin.view.transaction')->with($notification);

		      }else{
		        $notification=array(
		             'messege'=>'Try Again!',
		             'alert-type'=>'error'
		              );
		        return redirect()->route('admin.view.transaction')->with($notification);
		      }
		}

		elseif($status == 'cancelled')
		{
			$data['status'] = 3;
			$success = DB::table('transactions')->where('id',$id)->update($data);

			if($success){

		      $notification=array(
		             'messege'=>'Successfully Update!',
		             'alert-type'=>'success'
		              );
		        return redirect()->route('admin.view.transaction')->with($notification);

		      }else{
		        $notification=array(
		             'messege'=>'Try Again!',
		             'alert-type'=>'error'
		              );
		        return redirect()->route('admin.view.transaction')->with($notification);
		      }
		}
		elseif($status == 'success')
		{
			$data['status'] = 4;
			$success = DB::table('transactions')->where('id',$id)->update($data);

			if($success){

		      $notification=array(
		             'messege'=>'Successfully Update!',
		             'alert-type'=>'success'
		              );
		        return redirect()->route('admin.view.transaction')->with($notification);

		      }else{
		        $notification=array(
		             'messege'=>'Try Again!',
		             'alert-type'=>'error'
		              );
		        return redirect()->route('admin.view.transaction')->with($notification);
		      }
		}
		else{
			$notification=array(
	             'messege'=>'Select One!',
	             'alert-type'=>'error'
	              );
	        return redirect()->route('admin.view.transaction')->with($notification);
		}

		
	}


}
