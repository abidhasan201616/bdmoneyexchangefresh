<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use DB;

class LoginController extends Controller
{
    
  // public function __construct()
  //   {
  //       $this->middleware('auth');
  //   }

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/admin/route';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {  
        $inputVal = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        if(auth()->attempt(array('email' => $inputVal['email'], 'password' => $inputVal['password']))){
            if (auth()->user()->is_admin == 1) {
              
                return redirect()->route('admin.route');
            }else{
                
                return redirect()->route('home');
            }
        }else{
            return redirect()->route('home')
                ->with('error','Email & Password are incorrect.');
        }     
    }


    public function logout(Request $request) {
      Auth::logout();
      // $reserve = DB::table('our_reserveds')->get();
      return redirect()->route('home');
    }


    public function LoginUserForm()    {
        
        return view('admin.login');
    }
}