<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     return view('frontend.home');
    // }
    public function index(){
        $reserve = DB::table('our_reserveds')->get();
        $review = DB::table('reviews')->where('status',1)->get();

        return view('UserHomeContent.UserHomeContent',compact('reserve','review'));
    }

     public function handleAdmin()
    {
        return view('admin.admin_home');
    }   

   
}
