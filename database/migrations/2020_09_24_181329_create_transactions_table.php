<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');

            $table->string('order_id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('address_one');
            $table->string('address_two');
            $table->string('sender_account');
            $table->string('txdId');
            $table->string('receiver_account');
            
            $table->string('send_method');
            $table->string('receive_method');
            $table->integer('send_amount');
            $table->integer('receive_amount');
            $table->integer('status');
                     

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
