@extends('UserFrontend.home')


@section('content')
@php 
$currency_rate = DB::table('currency_rates')->where('status',1)->get();
@endphp
 
    <!--   News Start Here   -->
    <div class="section">
        <div class="row">
           
                <div class="col-sm-12 col-md-12 col-lg-12">
                   
                    <ul class="list-group">
                            
                          <li class="list-group-item">
                              <a href="#" > 
                                  <marquee behavior="" direction=""><h5 class="d-inline">NEWS UPDATE : </h5>
                                  	<ul> 
                                  		<li>This is news!</li>
                                  		<li>This is news!</li>
                                  		<li>This is news!</li>
                                  	</ul>

                                  </marquee>
                              </a>
                          </li>
                      </ul>
                    
                </div>
            
        </div>
    </div>
    
    <!--   News End Here   -->
    
    
    <!--   Main Start Here   -->
    <section class="main_section">
       <div class="container">
           <div class="row">
               <div class="col-sm-8 col-md-8 col-lg-8">
                   
                   <!--Main Left Going here-->
                    <div class="main_left_side mt-2">
                       <!--Card Going here-->
                    
                      
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="card mt-2">
                                   <div class="card-body">

                                      <ul class="nav nav-tabs" id="myTab" role="tablist">
										  <li class="nav-item" role="presentation">
										    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
										  </li>

										  <li class="nav-item" role="presentation">
										    <a class="nav-link" id="wallet-tab" data-toggle="tab" href="#wallet" role="tab" aria-controls="wallet" aria-selected="false">Wallet</a>
										  </li>

										  <li class="nav-item" role="presentation">
										    <a class="nav-link" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false">Settings</a>
										  </li>
										</ul>

										<div class="tab-content" id="myTabContent">

										  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
										  	<div class="row">
										  		<div class="col-sm-12 col-md-12 col-lg-12">
										  			<br><br>

										  			@if($profile->user_id == Auth::user()->id)
										  	<div class="row">
										  		<div class="col-sm-12 col-md-12 col-lg-12">
										  			<br><br>

										  			<form action="{{ URL::to('update/profile/'.$profile->id) }}" method="post">
										  				@csrf
													  <div class="form-group row">

													    <label for="staticEmail" class="col-sm-3 col-form-label">Name</label>

													    <div class="col-sm-9">
													      
													      <input type="text" name="name" class="form-control" id="name" value="{{ Auth::user()->name }}" readonly="">
													   

													    </div>
													  </div>

													  <div class="form-group row">

													    <label for="email" class="col-sm-3 col-form-label">Email</label>

													    <div class="col-sm-9">
													      <input type="email" name="email" class="form-control" id="email" value="{{ Auth::user()->email }}" readonly="">
													    </div>

													  </div>

													  <div class="form-group row">

													    <label for="address_one" class="col-sm-3 col-form-label">Address Line 1</label>

													    <div class="col-sm-9">
													      <input type="text" name="address_one" class="form-control" id="address_one" value="{{ $profile->address_one }}" placeholder="Address Line 1">
													    </div>

													  </div>

													  <div class="form-group row">

													    <label for="address_two" class="col-sm-3 col-form-label">Address Line 2</label>

													    <div class="col-sm-9">
													      <input type="text" name="address_two" class="form-control" id="address_two" value="{{ $profile->address_two }}" placeholder="Address Line 2">
													    </div>

													  </div>

													  <div class="form-group row">

													    <label for="city" class="col-sm-3 col-form-label">City</label>

													    <div class="col-sm-9">
													      <input type="text" name="city" class="form-control" id="city" value="{{ $profile->city }}" placeholder="City">
													    </div>

													  </div>

													  <div class="form-group row">

													    <label for="state" class="col-sm-3 col-form-label">State/Provice</label>

													    <div class="col-sm-9">
													      <input type="text" name="state" class="form-control" id="state" value="{{ $profile->state }}" placeholder="State">
													    </div>

													  </div>

													  <div class="form-group row">

													    <label for="zip_code" class="col-sm-3 col-form-label">Zip Code</label>

													    <div class="col-sm-9">
													      <input type="number" name="zip_code" class="form-control" id="zip_code" value="{{ $profile->zip }}" placeholder="Zip Code">
													    </div>

													  </div>

													   <div class="form-group row">

													    <label for="post_code" class="col-sm-3 col-form-label">Post Code</label>

													    <div class="col-sm-9">
													      <input type="number" name="post_code" class="form-control" id="post_code" value="{{ $profile->post_code }}" placeholder="Post Code">
													    </div>

													  </div>

													  <div class="form-group row">

													    <label for="country" class="col-sm-3 col-form-label">Country</label>

													    <div class="col-sm-9">
													      <input type="text" name="country" class="form-control" id="country" value="{{ $profile->country }}" placeholder="Country">
													    </div>

													  </div>

													  <div class="form-group row">

													    <label for="phone" class="col-sm-3 col-form-label">Phone</label>

													    <div class="col-sm-9">
													      <input type="text" name="phone" class="form-control" id="phone" value="{{ $profile->phone }}" placeholder="Phone">
													    </div>

													  </div>

													  <div class="form-group row">											  
													  	<div class="col-md-3">
													  	</div>
													    <div class="col-md-9">
													      <button type="submit" class="form-control btn btn-info shadow">Update</button>
													    </div>

													  </div>


													</form>


										  		</div>
										  		
										  	</div>
										  	@else
										  	<form action="{{ route('store.profile.data') }}" method="post">
								  				@csrf
											  <div class="form-group row">

											    <label for="staticEmail" class="col-sm-3 col-form-label">Name</label>

											    <div class="col-sm-9">
											      
											      <input type="text" name="name" class="form-control" id="name" value="{{ Auth::user()->name }}" readonly="" required="">
											   

											    </div>
											  </div>

											  <div class="form-group row">

											    <label for="email" class="col-sm-3 col-form-label">Email</label>

											    <div class="col-sm-9">
											      <input type="email" name="email" class="form-control" id="email" value="{{ Auth::user()->email }}" readonly="" required="">
											    </div>

											  </div>

											  <div class="form-group row">

											    <label for="address_one" class="col-sm-3 col-form-label">Address Line 1</label>

											    <div class="col-sm-9">
											      <input type="text" name="address_one" class="form-control" id="address_one" placeholder="Address Line 1" required="">
											    </div>

											  </div>

											  <div class="form-group row">

											    <label for="address_two" class="col-sm-3 col-form-label">Address Line 2</label>

											    <div class="col-sm-9">
											      <input type="text" name="address_two" class="form-control" id="address_two" placeholder="Address Line 2" required="">
											    </div>

											  </div>

											  <div class="form-group row">

											    <label for="city" class="col-sm-3 col-form-label">City</label>

											    <div class="col-sm-9">
											      <input type="text" name="city" class="form-control" id="city" placeholder="City" required="">
											    </div>

											  </div>

											  <div class="form-group row">

											    <label for="state" class="col-sm-3 col-form-label">State/Provice</label>

											    <div class="col-sm-9">
											      <input type="text" name="state" class="form-control" id="state" placeholder="State" required="">
											    </div>

											  </div>

											  <div class="form-group row">

											    <label for="zip_code" class="col-sm-3 col-form-label">Zip Code</label>

											    <div class="col-sm-9">
											      <input type="number" name="zip_code" class="form-control" id="zip_code" placeholder="Zip Code" required="">
											    </div>

											  </div>

											   <div class="form-group row">

											    <label for="post_code" class="col-sm-3 col-form-label">Post Code</label>

											    <div class="col-sm-9">
											      <input type="number" name="post_code" class="form-control" id="post_code" placeholder="Post Code" required="">
											    </div>

											  </div>

											  <div class="form-group row">

											    <label for="country" class="col-sm-3 col-form-label">Country</label>

											    <div class="col-sm-9">
											      <input type="text" name="country" class="form-control" id="country" placeholder="Country" required="">
											    </div>

											  </div>

											  <div class="form-group row">

											    <label for="phone" class="col-sm-3 col-form-label">Phone</label>

											    <div class="col-sm-9">
											      <input type="text" name="phone" class="form-control" id="phone" placeholder="Phone" required="">
											    </div>

											  </div>

											  <div class="form-group row">											  
											  	<div class="col-md-3">
											  	</div>
											    <div class="col-md-9">
											      <button type="submit" class="form-control btn btn-info shadow">Submit</button>
											    </div>

											  </div>


											</form>
										  	@endif


										  			


										  		</div>
										  		
										  	</div>



										  </div>
										{{-- Profile edit form start --}}
										  <div class="tab-pane fade" id="wallet" role="tabpanel" aria-labelledby="wallet-tab">


										  	Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptas distinctio sit odio, repellendus hic obcaecati magnam in, soluta repellat magni quae quam similique corporis, nostrum officia recusandae! Iusto, eius nihil.

										  	


										  </div>
										{{-- Profile edit form end --}}

										  <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="settings-tab">
										  	Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Incidunt quis officiis architecto voluptatem dicta voluptates minus voluptatum quidem, impedit dolore rem eius necessitatibus ducimus, ut aliquid sapiente tempore, sequi nesciunt!
										  </div>
										</div>
                                      
                                       
                                   </div>
                               </div>
                               
                               
                               
                           </div>
                       </div>
                       
                       
                    </div> <!--Main Left End-->
                    
                   
               </div>
               
               <div class="col-sm-4 col-md-4 col-lg-4">
                   <div class="main_right_side mt-3">

                       <div class="card">
                       		@if($pro_photo = Auth::user()->photo)
                       		<img src="{{ url($pro_photo) }}" height="100" class="card-img-top img-fluid" alt="Profile Photo">
                       		@else
                       		<img src="{{ asset('public/img/avatar.jpg') }}" height="100" class="card-img-top img-fluid" alt="Profile Photo">
                       		@endif

						  

						  <div class="card-body">
						    <h5 class="card-title">

						    	{{ Auth::user()->name }}
						    
							</h5>
						    
						  </div>
						  <ul class="list-group list-group-flush">
						    <form action="{{ URL::to('upload/photo/'.Auth::user()->id) }}" method="post" enctype="multipart/form-data">
						    	@csrf
							    <li class="list-group-item"><div class="custom-file">
								  <input type="file" class="custom-file-input" name="photo" id="customFile">
								  <label class="custom-file-label" for="customFile">Choose file</label>
								</div>
								</li>
								<li class="list-group-item">
									<button type="submit" class="btn btn-primary">Upload</button>
								</li>
							</form>

						    
						    <li class="list-group-item">Vestibulum at eros</li>
						  </ul>
						  <div class="card-body">
						    <a href="#" class="card-link">Card link</a>
						    <a href="#" class="card-link">Another link</a>
						  </div>
						</div>
                       
                
                       
                                              
                   </div> <!--Right side End here-->
                   
               </div><!--Col End here-->
           </div><!--Row End here-->
           
       </div><!--Container End here-->
        
    </section>   <!--   News Main End Here   -->
    
    
  
@endsection