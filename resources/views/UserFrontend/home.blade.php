<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>BD Money Exchange</title>
        
        @php 
        $brand = DB::table('brand_logos')->where('status',1)->first();
        @endphp

        @include('partials.link')

  <!-- Custom styles for this template-->
  		
     
    </head>
    <body style="background: #373797;">


	    <!--   Header Start Here   -->
   <header class="header p-2">
       <div class="container">
           <div class="row justify-content-between">
                <div class="col-4 ">                             
                   <div class="card-body">        
                        
                    </div> 
                </div>
                <div class="col-4 ">
                             
                   <div class="card-body text-right"> 
                   @if(Auth::check()) 

                   <a href="{{ URL::to('/logout') }}" type="button" class="btn btn-light pl-4 pr-3" id="login">Logout</a>
                   @else
                  <a href="{{ route('login') }}" type="button" class="btn btn-light pl-4 pr-3" id="login">Login</a>                       
                  <a href="{{ route('register') }}" type="button" class="btn btn-light">Register</a>

                    @endif

                    </div> 
                </div>
            </div>
               
           </div>
      
       
      
   </header>
   <!--   Header End Here   -->
      
<!--   Navbar Start Here   -->


<nav class="navbar navbar-expand-lg bg-color-custom">
    <a class="navbar-brand" href="#">
      @if($brand)
        <img src="{{ url($brand->brand_logo) }}" width="70" alt="">
      @else
        <img src="{{ asset('public/assets/AdminBackend/img/logo.png') }}" width="70" alt="">
      @endif

        
    </a>
  <button class="navbar-toggler border" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<!--    <span class="navbar-toggler-icon btn btn-success"></span>-->
 <span class="text-warning"><b>Menu</b></span>
  </button>

  <div class="collapse navbar-collapse " id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto  text-uppercase font-weight-bold">
@php 
$menu = DB::table('header_menues')->where('status',1)->get();
@endphp
      @foreach($menu as $row)
        <li class="nav-item">
          <a class="nav-link" href="{{ URL::to('/') }}">{{ $row->menu_item }} <span class="sr-only">(current)</span></a>
        </li>
      @endforeach
    
      @if(Auth::check())
      <div class="btn-group dropleft">
      <button type="button" class="btn dropdown-toggle text-white profile_btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span style="font-size: 20px; font-weight: bold;">{{Auth::user()->name }}</span>
      </button>
      <div class="dropdown-menu">
         <a class="dropdown-item" href="{{ route('profile') }}">Profile</a>
        <a class="dropdown-item" href="#">Another action</a>
        <a class="dropdown-item" href="#">Something else here</a>
      </div>
    </div> 
      @else
      

       

      @endif  
        
    </ul>
   
  </div>
</nav>
<!--   Navbar End Here   -->

        @yield('content')    
     

      @include('partials.footer')
      @include('partials.script')

    </body>
</html>
