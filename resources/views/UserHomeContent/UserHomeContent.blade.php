@extends('UserFrontend.home')


@section('content')
@php 
$currency_rate = DB::table('currency_rates')->where('status',1)->get();
$send_method = DB::table('sends')->where('status',1)->get();
$receive_method = DB::table('receives_methods')->where('status',1)->get();
@endphp
 
    <!--   News Start Here   -->
    <div class="section">
        <div class="row">
           
                <div class="col-sm-12 col-md-12 col-lg-12">
                   
                    <ul class="list-group">
                            
                          <li class="list-group-item">
                              <a href="#" > 
                                  <marquee behavior="" direction="">
                                    

                                    <div class="news_title_box d-flex">
                                      <div class="title"><h5 class="d-inline">NEWS UPDATE : </h5></div>
                                      <div class="title ml-2 mr-2"><i class="fab fa-buffer"></i> This is news!</div>
                                      <div class="title ml-2 mr-2"><i class="fab fa-buffer"></i> This is news!</div>
                                      <div class="title ml-2 mr-2"> <i class="fab fa-buffer"></i> This is news!</div>
                                    </div>
                                  

                                  </marquee>
                              </a>
                          </li>
                      </ul>
                    
                </div>
            
        </div>
    </div>
    
    <!--   News End Here   -->
    
    
    <!--   Main Start Here   -->
    <section class="main_section">
       <div class="container">
           <div class="row">
               <div class="col-sm-8 col-md-8 col-lg-8">
                   
                   <!--Main Left Going here-->
                    <div class="main_left_side mt-2">
                       <!--Card Going here-->
                       <form action="{{ URL::to('select/method') }}" method="get">
                        @csrf
                       <div class="card">
                           <div class="card-body">
                               <h2 class="text-center text-success"><i class="fab fa-gg-circle"></i> Exchange</h2>


                               <hr>
                              {{-- Select Method Start --}}
                              <div class="row mt-2 mb-2">
                                
                                  
                                  <div class="col-sm-5 col-md-5 col-lg-5">
                                      <div class="send_method">
                                          <select class="input-group p-2" name="send_method_name" required>
                                             <option class="input-group-item p-2" value="">Select Method</option>
                                            @foreach($send_method as $row)
                                              <option class="input-group-item p-2" value="{{$row->send_method_name}}">
                                                {{ $row->send_method_name }}
                                              
                                              </option>
                                            @endforeach
                                          </select>
                                      </div>
                                  </div>
                                  
                                  <div class="col-sm-2 col-md-2 col-lg-2">
                                      <div class="exchange m-auto text-success text-center pt-1">
                                          <i class="fas fa-arrows-alt-h "></i>
                                      </div>
                                  </div>
                                  
                                  
                                  <div class="col-sm-5 col-md-5 col-lg-5">
                                      <div class="receive_method">
                                          <select class="input-group p-2" name="receive_method_name" required>
                                             <option class="input-group-item p-2" value="">Select Method</option>
                                            
                                            @foreach($receive_method as $row) 
                                              <option class="input-group-item p-2" value="{{ $row->receive_method_name }}">
                                              {{ $row->receive_method_name }}
                                              
                                              </option>
                                            @endforeach
                                          </select>
                                      </div>
                                  </div>                                  
                                                         
                              </div><!--Row End here-->
                              {{-- Select Method End --}}

                              {{-- Enter Amount Start --}}
                              <div class="row mt-2 mb-2">
                                
                                  
                                  <div class="col-sm-5 col-md-5 col-lg-5">
                                      <div class="send_method">                                       

                                       
                                          <input type="number" class="input-group p-2" name="sent_amount" id="sent_amount" placeholder="0.00" required="">

                                      </div>
                                  </div>
                                  
                                  <div class="col-sm-2 col-md-2 col-lg-2">
                                      <div class="exchange m-auto text-success text-center pt-1">
                                          <i class="fas fa-arrows-alt-h "></i>
                                      </div>
                                  </div>
                                  
                                  
                                  <div class="col-sm-5 col-md-5 col-lg-5">
                                      <div class="receive_method">
                                         <input type="number" class="input-group p-2" name="receive_amount" id="receive_amount" placeholder="0.00" required="">
                                      </div>
                                  </div>                                  
                                                         
                              </div><!--Row End here-->
                              {{-- Enter Amount End --}}
                              
                              <hr>
                              <div class="container">
                                  <div class="row">
                                      <div class="col-md-6 offset-md-3">
                                           <div class="exchange_btn text-center">
                                              <button type="submit" class="btn btn-light"><h5 style="margin: 0px;">  Next <i class="fas fa-chevron-right"></i> </h5></button>
                                            
                                          </div>
                                      </div><!--Col-md-6 End here-->
                                  </div><!--Row End here-->
                              </div><!--Container End here-->
                             
                            
                              
                           </div><!--Card Body End here-->
                           
                       </div><!--Card End here-->
                  </form> 
                        <div class="row mt-2">
                           <div class="col-sm-12 col-md-12 col-lg-12">

                              <div class="card">
                                  <div class="card-body">
                                      <div class="from_admin">
                                           <h5>আসসালামু আলাইকুম, OurexBD সকল ইউজার কে শুভেচ্ছা ও স্বাগতম। আমাদের OurexBD সাইট আপডেট করা হয়েছে আপনাদের সুভিধার জন্য। OurexBD সকল ইউজারদেরকে Dollar Buy Sell করার আগে নতুন করে একটা ACCOUNT করে নিতে হবে। সততার সাথে কাজ করে চলছে OurexBD। যে কনো প্রয়োজন এ আমাদের Support Number কল করুন। আমাদের ওয়েবসাইট ছারা অন্ন কনো মাধ্যমে লেনদেন করিনা। ধন্যবাদ আমাদের সাথে থাকার জন্য।</h5>
                                       </div><!--From admin End here-->
                                  </div><!--Card Body End here-->
                              </div><!--Card End here-->


                           </div>
                       </div>
                      
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="card mt-2">
                                   <div class="card-body">
                                      <div class="exchange_head text-center">
                                          <h2 class="text-success"> <i class="far fa-clock"></i> Latest Exchange  <div class="spinner-grow text-primary" role="status">
                                          <span class="sr-only">Loading...</span>
                                        </div></h2>
                                      </div>
                                      
                                      <div class="latest_exchange">
                                          <table class="table table-responsive table-hover">
                                              <thead class="thead-light">
                                                <tr>
                                                  <th scope="col">SL</th>     
                                                  <th scope="col">Send</th>
                                                  <th scope="col">Received</th>
                                                  <th scope="col">User</th>
                                                  <th scope="col">Date And Time</th>
                                                  <th scope="col">Status</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                               
                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-success">Success</span>
                                                    
                                                  </td>
                                                </tr>
                                                
                                                 <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>                                                  
                                                    <span class="badge badge-danger">Cancelled</span>
                                                  </td>
                                                </tr>
                                                
                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-primary">Proccessing</span>
                                                   
                                                  </td>
                                                </tr>
                                                
                                                 <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>
                                                
                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>


                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>

                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>


                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>


                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>
                                                
                                              
                                         
                                           
                                                
                                                
                                                
                                              </tbody>
                                            </table>
                                      </div>
                                      
                                       
                                   </div>
                               </div>
                               
                               
                               
                           </div>
                       </div>
                       
                       
                    </div> <!--Main Left End-->
                    
                   
               </div>
               
               <div class="col-sm-4 col-md-4 col-lg-4">
                   <div class="main_right_side mt-2">
                       <div class="card">
                           <div class="card-body">
                               <h4 class="text-center text-success"> <i class="fas fa-coins"></i> Our Reserved</h4>
                               <hr>
                               
                               <table class="table text-center  w-100">
                                  <thead class="thead-dark">
                                    <tr>
                                      <th scope="col">Method</th>
                                      <th scope="col">Amount</th>                                      
                                      <th scope="col">Currency</th>                                      
                                    </tr>
                                  </thead>
                                  
                                  <tbody>

                                    @foreach($reserve as $row)
                                   
                                    <tr>
                                      <th scope="row"><img src="{{url($row->logo)}}" width="30" height="30" alt="">{{ $row->method_name }}</th>
                                      <td>{{ $row->reserve_amount }}</td>
                                      <td>{{ Str::upper($row->currency_name) }}</td>
                                     
                                    </tr>
                                    @endforeach
                                    
                                 
                                      
                                    
                                    
                                   
                                  </tbody>
                                  
                                </table>
                           </div>
                       </div>
                       
                    <!-- Rate Table going here-->
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="card mt-2">
                                   <div class="card-body">
                                       <div class="rate text-center text-success">
                                           <h4> <i class="fas fa-signal"></i> Exchange Rate</h4>
                                       </div>
                                       

                                       <div class="rate_table">
                                           
                                           <table class="table text-center">
                                              <thead class="thead-dark">
                                                <tr>
                                                  <th scope="col">Currency</th>
                                                  <th scope="col">We Buy</th>                                   
                                                  <th scope="col">We Sell</th>                                      
                                                </tr>
                                              </thead>

                                              <tbody>
                                              @foreach($currency_rate as $row)

                                              
                                                <tr>
                                                  <th scope="row">
                                                    <img src="{{url($row->buy_currency_sign)}}" width="30" height="30" alt="">{{ $row->buy_currency_name}}</th>
                                                  <td>{{ $row->we_buy}}</td>
                                                  <td>{{ $row->we_sell}}</td>

                                                </tr>

                                                @endforeach
                                                
                                               
                                               </tbody>
                                      
                                           </table>
                                       </div><!--Rate table End here-->
                                       
                                       
                                   </div><!--Card body End here-->
                               </div><!--Card End here-->
                           </div><!--Col End here-->
                       </div><!--Row End here-->
                       
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="card mt-2">
                                   <div class="card-body">
                                       <div class="rate text-center text-success">
                                           <h4> <i class="fas fa-smile-beam"></i> Our Review</h4>
                                       </div>
                                       
                                       <div class="review">
                                           <div class="parent-box owl-carousel">
                                                
                                                {{-- Review item start --}}
                                                @foreach($review as $row)
                                                   <div class="item">
                                                        <div class="card">

                                                          <div class="card-body">
                                                            <h5 class="card-title">

                                                              <img src="{{asset('public/assets/UserFrontend/img/avatar.jpg')}}" width="30" height="150" alt="">{{ $row->name }}</h5>

                                                            <p class="card-text">
                                                              {{ Str::words($row->review,10) }}
                                                             </p>
                                                              

                                                          </div>
                                                        </div>

                                                    </div>
                                                  @endforeach
                                                    {{-- Review item end --}}
                                      
                                              

                                            </div>
                                           
                                           
                                       </div><!--Review End here-->
                                       
                                       
                                   </div><!--Card body End here-->
                               </div><!--Card End here-->
                           </div><!--Col End here-->
                       </div><!--Row End here-->
                       
                       
                   </div> <!--Right side End here-->
                   
               </div><!--Col End here-->
           </div><!--Row End here-->
           
       </div><!--Container End here-->
        
    </section>   <!--   News Main End Here   -->
    
    
  
@endsection