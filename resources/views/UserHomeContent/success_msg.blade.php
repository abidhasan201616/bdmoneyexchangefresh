@extends('UserFrontend.home')


@section('content')
@php 
$currency_rate = DB::table('currency_rates')->where('status',1)->get();
$send_method = DB::table('sends')->where('status',1)->get();
$receive_method = DB::table('receives_methods')->where('status',1)->get();
@endphp
 
    <!--   News Start Here   -->
    <div class="section">
        <div class="row">
           
                <div class="col-sm-12 col-md-12 col-lg-12">
                   
                    <ul class="list-group">
                            
                          <li class="list-group-item">
                              <a href="#" > 
                                  <marquee behavior="" direction="">
                                    

                                    <div class="news_title_box d-flex">
                                      <div class="title"><h5 class="d-inline">NEWS UPDATE : </h5></div>
                                      <div class="title ml-2 mr-2"><i class="fab fa-buffer"></i> This is news!</div>
                                      <div class="title ml-2 mr-2"><i class="fab fa-buffer"></i> This is news!</div>
                                      <div class="title ml-2 mr-2"> <i class="fab fa-buffer"></i> This is news!</div>
                                    </div>
                                  

                                  </marquee>
                              </a>
                          </li>
                      </ul>
                    
                </div>
            
        </div>
    </div>
    
    <!--   News End Here   -->
    
    
    <!--   Main Start Here   -->
    <section class="main_section">
       <div class="container">
           <div class="row">
               <div class="col-sm-12 col-md-12 col-lg-12">
                   
                   <!--Main Left Going here-->
                    <div class="main_left_side mt-2">
                       <!--Card Going here-->
                       <div class="card">
                       
                           <div class="card-body">
                           
                           <div class="alert alert-success" role="alert">
							 আমরা আপনার Transaction টি রিসিভ করেছি। এখন এটি Pending আছে। আমরা কিছুক্ষণের মধ্যে Transaction ID চেক করে আপডেট দিবো। আপডেট আমাদের Home Page এ অথবা আপনার Profile এ দেখতে পারবেন। ধন্যবাদ।
							</div>
							  
							
                           </div><!--Card Body End here-->
                           
                       </div><!--Card End here-->
                  
                      
                       
                       
                    </div> <!--Main Left End-->
                    
                   
               </div>
               



           </div><!--Row End here-->
           
       </div><!--Container End here-->
        
    </section>   <!--   News Main End Here   -->
    
    
  
@endsection