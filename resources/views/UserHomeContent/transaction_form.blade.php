@extends('UserFrontend.home')


@section('content')
@php 
$currency_rate = DB::table('currency_rates')->where('status',1)->get();
$send_method = DB::table('sends')->where('status',1)->get();
$receive_method = DB::table('receives_methods')->where('status',1)->get();
@endphp
 
    <!--   News Start Here   -->
    <div class="section">
        <div class="row">
           
                <div class="col-sm-12 col-md-12 col-lg-12">
                   
                    <ul class="list-group">
                            
                          <li class="list-group-item">
                              <a href="#" > 
                                  <marquee behavior="" direction="">
                                    

                                    <div class="news_title_box d-flex">
                                      <div class="title"><h5 class="d-inline">NEWS UPDATE : </h5></div>
                                      <div class="title ml-2 mr-2"><i class="fab fa-buffer"></i> This is news!</div>
                                      <div class="title ml-2 mr-2"><i class="fab fa-buffer"></i> This is news!</div>
                                      <div class="title ml-2 mr-2"> <i class="fab fa-buffer"></i> This is news!</div>
                                    </div>
                                  

                                  </marquee>
                              </a>
                          </li>
                      </ul>
                    
                </div>
            
        </div>
    </div>
    
    <!--   News End Here   -->
    
    
    <!--   Main Start Here   -->
    <section class="main_section">
       <div class="container">
           <div class="row">
               <div class="col-sm-8 col-md-8 col-lg-8">
                   
                   <!--Main Left Going here-->
                    <div class="main_left_side mt-2">
                       <!--Card Going here-->
                       <div class="card">
                       	<form action="{{ URL::to('store/transaction') }}" method="post">
				  			@csrf
                           <div class="card-body">
                               <h2 class="text-center text-success"><i class="fab fa-gg-circle"></i> Transaction</h2>

                             
                             <hr>
                             @php
                             $GenerateOrderId = strtoupper(uniqid());

                             @endphp

                             

				  				{{-- select method data from home page going here --}}
				  				@php
				  					$send_method = $data['send_method'];
				  					$receive_method = $data['receive_method'];
				  					$send_amount = $data['send_amount'];
				  					$receive_amount = $data['receive_amount'];

				  					$usd = 'USD';
				  					$tk = 'TK';

				  				@endphp

				  				<input type="hidden" name="send_method" value="{{ $send_method  }}">
				  				<input type="hidden" name="receive_method" value="{{ $receive_method  }}">
				  				<input type="hidden" name="send_amount" value="{{ $send_amount  }}">
				  				<input type="hidden" name="receive_amount" value="{{ $receive_amount  }}">
				  				{{-- select method data from home page End here --}}

								<div class="form-group row">

									<label for="order_id" class="col-sm-3 col-form-label">Order ID</label>

									<div class="col-sm-9">
									<input type="text" name="order_id" class="form-control" id="order_id" value="{{  $GenerateOrderId }}" readonly="">
									</div>

								</div>

								<div class="form-group row">

								    <label for="totalSend" class="col-sm-3 col-form-label">Total Send Amount</label>
								    
								    <div class="col-sm-9">

								    @if($send_method == 'Bkash')

								       <input type="text" name="totalSend" class="form-control" id="totalSend" value="{{ $send_amount.' '.$tk  }}" readonly="">
								      
								    @elseif($send_method == 'Nagad')
										<input type="text" name="totalSend" class="form-control" id="totalSend" value="{{ $send_amount.' '.$tk  }}" readonly="">

									@elseif($send_method == 'Rocket')
										<input type="text" name="totalSend" class="form-control" id="totalSend" value="{{ $send_amount.' '.$tk  }}" readonly="">

									@elseif($send_method == 'Payoneer')
										<input type="text" name="totalSend" class="form-control" id="totalSend" value="{{ $send_amount.' '.$usd  }}" readonly="">

									@elseif($send_method == 'Paypal')
										<input type="text" name="totalSend" class="form-control" id="totalSend" value="{{ $send_amount.' '.$usd  }}" readonly="">
									@elseif($send_method == 'Skrill')
										<input type="text" name="totalSend" class="form-control" id="totalSend" value="{{ $send_amount.' '.$usd  }}" readonly="">

									@elseif($send_method == 'Webmoney')
										<input type="text" name="totalSend" class="form-control" id="totalSend" value="{{ $send_amount.' '.$usd  }}" readonly="">
									@else


								    @endif
								     					   

								    </div>							  
							  </div>

							  <div class="form-group row">

							    <label for="totalReceive" class="col-sm-3 col-form-label">Total Receive Amount</label>
							    
							    <div class="col-sm-9">
							      
							     
							    @if($receive_method == 'Bkash')

							       <input type="text" name="totalReceive" class="form-control" id="totalReceive" value="{{ $receive_amount.' '.$tk  }}" readonly="">
							      
							    @elseif($receive_method == 'Nagad')
									<input type="text" name="totalReceive" class="form-control" id="totalReceive" value="{{ $receive_amount.' '.$tk  }}" readonly="">

								@elseif($receive_method == 'Rocket')
									<input type="text" name="totalReceive" class="form-control" id="totalReceive" value="{{ $receive_amount.' '.$tk  }}" readonly="">

								@elseif($receive_method == 'Payoneer')
									<input type="text" name="totalReceive" class="form-control" id="totalReceive" value="{{ $receive_amount.' '.$usd  }}" readonly="">

								@elseif($receive_method == 'Paypal')
									<input type="text" name="totalReceive" class="form-control" id="totalReceive" value="{{ $receive_amount.' '.$usd  }}" readonly="">
								@elseif($receive_method == 'Skrill')
									<input type="text" name="totalReceive" class="form-control" id="totalReceive" value="{{ $receive_amount.' '.$usd  }}" readonly="">

								@elseif($receive_method == 'Webmoney')
									<input type="text" name="totalReceive" class="form-control" id="totalReceive" value="{{ $receive_amount.' '.$usd  }}" readonly="">
								@else


							    @endif					   

							    </div>							  
							  </div>

								<div class="form-group row">

									<label for="agent_account" class="col-sm-3 col-form-label">Agent Account</label>

									<div class="col-sm-9">
									
									@if($data['send_method'] == 'Bkash')									

									01788397462 এই	Bkash Account এ টাকা পাঠান এবং আপনি যে Account এ Money রিসিভ করতে চান সেই Account No নিচের ফরমে পূরণ করুন।

									@elseif($data['send_method'] == 'Rocket')
										
									01788397462-2 এই Rocket Account এ টাকা পাঠান এবং আপনি যে Account এ Money রিসিভ করতে চান সেই Account No নিচের ফরমে পূরণ করুন।

									@elseif($data['send_method'] == 'Nagad')
									
									01788397462-2 এই Nagad Account এ টাকা পাঠান এবং আপনি যে Account এ Money রিসিভ করতে চান সেই Account No নিচের ফরমে পূরণ করুন।

									@elseif($data['send_method'] == 'Payoneer')
										
									5698745228 এই	Payoneer Account এ USD Doller পাঠান এবং আপনি যে Account এ Money রিসিভ করতে চান সেই Account No নিচের ফরমে পূরণ করুন।

									@elseif($data['send_method'] == 'Paypal')
										
									569877843228 এই	PayPal Account এ USD Doller পাঠান এবং আপনি যে Account এ Money রিসিভ করতে চান সেই Account No নিচের ফরমে পূরণ করুন।

									@elseif($data['send_method'] == 'Webmoney')
										
									569877843228 এই	Webmoney Account এ USD Doller পাঠান এবং আপনি যে Account এ Money রিসিভ করতে চান সেই Account No নিচের ফরমে পূরণ করুন।

									@elseif($data['send_method'] == 'Skrill')
										
									569877843228 এই	Skrill Account এ USD Doller পাঠান এবং আপনি যে Account এ Money রিসিভ করতে চান সেই Account No নিচের ফরমে পূরণ করুন।
								
									@else
									দয়া করে পূর্বের পেজে লেনদেনের মেথোড গুলো Select করুন।

									@endif

									


									</div>

								</div>

							  <div class="form-group row">

							    <label for="name" class="col-sm-3 col-form-label">Name</label>
							    @foreach($userData as $row)
							    <div class="col-sm-9">
							      
							      <input type="text" name="name" class="form-control" id="name" value="{{ $row->name }}" readonly="">
							   

							    </div>
							  
							  </div>

							  <div class="form-group row">

							    <label for="email" class="col-sm-3 col-form-label">Email</label>

							    <div class="col-sm-9">
							      <input type="email" name="email" class="form-control" id="email" value="{{ $row->email }}" readonly="">
							    </div>

							  </div>

							  <div class="form-group row">

							    <label for="phone" class="col-sm-3 col-form-label">Phone</label>

							    <div class="col-sm-9">
							      <input type="number" name="phone" class="form-control" id="phone" value="{{ $row->phone }}" readonly="">
							    </div>

							  </div>
							    

							  <div class="form-group row">

							    <label for="address_one" class="col-sm-3 col-form-label">Address Line 1</label>

							    <div class="col-sm-9">
							      <input type="text" name="address_one" class="form-control" id="address_one" value="{{ $row->address_one }}" readonly="">
							    </div>

							  </div>

							  <div class="form-group row">

							    <label for="address_two" class="col-sm-3 col-form-label">Address Line 2</label>

							    <div class="col-sm-9">
							      <input type="text" name="address_two" class="form-control" id="address_two" value="{{ $row->address_one }}" readonly="">
							    </div>

							  </div>


							  <div class="form-group row">

							    <label for="sender_account" class="col-sm-3 col-form-label">আপনি যে Account থেকে Money Send করেছেন</label>

							    <div class="col-sm-9">
							      <input type="text" name="sender_account" class="form-control" id="sender_account" placeholder="Enter sender account" required="">
							    </div>

							  </div>

							  <div class="form-group row">

							    <label for="txdId" class="col-sm-3 col-form-label">Money Send করার পর যে TxdId পেয়েছেন তা দিন</label>

							    <div class="col-sm-9">
							      <input type="text" name="txdId" class="form-control" id="txdId" placeholder="Enter Transaction ID">
							    </div>

							  </div>

							   <div class="form-group row">

							    <label for="receiver_account" class="col-sm-3 col-form-label">আপনি যে Account এ Money রিসিভ করতে চান</label>

							    <div class="col-sm-9">
							      <input type="text" name="receiver_account" class="form-control" id="receiver_account" placeholder="Enter receiver account" required="">
							    </div>

							  </div>

							 
							  
							  <div class="form-group row">

							    <label for="" class="col-sm-3 col-form-label">আপনি যে Account থেকে Exchange Money রিসিভ করবেন</label>

							    <div class="col-sm-9">

							    @if($data['receive_method'] == 'Bkash')
							    
							    01788397462 এই	Bkash Account থেকে Money পাবেন।

							    @elseif($data['receive_method'] == 'Rocket')
										
								01788397462-2 এই Rocket Account থেকে Money পাবেন।

								@elseif($data['receive_method'] == 'Nagad')
								
								01788397462-2 এই Nagad Account থেকে Money পাবেন।

								@elseif($data['receive_method'] == 'Payoneer')
									
								5698745228 এই	Payoneer Account থেকে Money পাবেন।

								@elseif($data['receive_method'] == 'Paypal')
									
								569877843228 এই	PayPal Account এ থেকে Money পাবেন।

								@elseif($data['receive_method'] == 'Webmoney')
									
								569877843228 এই	Webmoney Account এ থেকে Money পাবেন।

								@elseif($data['receive_method'] == 'Skrill')
									
								569877843228 এই	Skrill Account এ থেকে Money পাবেন।
							
								@else

								দয়া করে পূর্বের পেজে লেনদেনের মেথোড গুলো Select করুন।


							  @endif

							    </div>

							  </div>
							  			

							  @endforeach
								<hr>
							   <div class="form-group row">
							   
							    <div class="col-sm-6 offset-sm-3">
							      <button type="submit" class="form-control btn btn-info">Submit</button> 
							    </div>

							  </div>

							</form>
                           </div><!--Card Body End here-->
                           
                       </div><!--Card End here-->
                  
                      
                       
                       
                    </div> <!--Main Left End-->
                    
                   
               </div>
               
               <div class="col-sm-4 col-md-4 col-lg-4">
                   <div class="main_right_side mt-2">
                       <div class="card">
                           <div class="card-body">
                               <h4 class="text-center text-success"> <i class="fas fa-coins"></i> Our Reserved</h4>
                               <hr>
                               
                               <table class="table text-center  w-100">
                                  <thead class="thead-dark">
                                    <tr>
                                      <th scope="col">Method</th>
                                      <th scope="col">Amount</th>                                      
                                      <th scope="col">Currency</th>                                      
                                    </tr>
                                  </thead>
                                  
                                  <tbody>

                                    @foreach($reserve as $row)
                                   
                                    <tr>
                                      <th scope="row"><img src="{{url($row->logo)}}" width="30" height="30" alt="">{{ $row->method_name }}</th>
                                      <td>{{ $row->reserve_amount }}</td>
                                      <td>{{ Str::upper($row->currency_name) }}</td>
                                     
                                    </tr>
                                    @endforeach
                                    
                                 
                                      
                                    
                                    
                                   
                                  </tbody>
                                  
                                </table>
                           </div>
                       </div>
                       
                    <!-- Rate Table going here-->
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="card mt-2">
                                   <div class="card-body">
                                       <div class="rate text-center text-success">
                                           <h4> <i class="fas fa-signal"></i> Exchange Rate</h4>
                                       </div>
                                       

                                       <div class="rate_table">
                                           
                                           <table class="table text-center">
                                              <thead class="thead-dark">
                                                <tr>
                                                  <th scope="col">Currency</th>
                                                  <th scope="col">We Buy</th>                                   
                                                  <th scope="col">We Sell</th>                                      
                                                </tr>
                                              </thead>

                                              <tbody>
                                              @foreach($currency_rate as $row)

                                              
                                                <tr>
                                                  <th scope="row">
                                                    <img src="{{url($row->buy_currency_sign)}}" width="30" height="30" alt="">{{ $row->buy_currency_name}}</th>
                                                  <td>{{ $row->we_buy}}</td>
                                                  <td>{{ $row->we_sell}}</td>

                                                </tr>

                                                @endforeach
                                                
                                               
                                               </tbody>
                                      
                                           </table>
                                       </div><!--Rate table End here-->
                                       
                                       
                                   </div><!--Card body End here-->
                               </div><!--Card End here-->
                           </div><!--Col End here-->
                       </div><!--Row End here-->
                       
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="card mt-2">
                                   <div class="card-body">
                                       <div class="rate text-center text-success">
                                           <h4> <i class="fas fa-smile-beam"></i> Our Review</h4>
                                       </div>
                                       
                                       <div class="review">
                                           <div class="parent-box owl-carousel">
                                                
                                                {{-- Review item start --}}
                                                @foreach($review as $row)
                                                   <div class="item">
                                                        <div class="card">

                                                          <div class="card-body">
                                                            <h5 class="card-title">

                                                              <img src="{{asset('public/assets/UserFrontend/img/avatar.jpg')}}" width="30" height="150" alt="">{{ $row->name }}</h5>

                                                            <p class="card-text">
                                                              {{ Str::words($row->review,10) }}
                                                             </p>
                                                              

                                                          </div>
                                                        </div>

                                                    </div>
                                                  @endforeach
                                                    {{-- Review item end --}}
                                      
                                              

                                            </div>
                                           
                                           
                                       </div><!--Review End here-->
                                       
                                       
                                   </div><!--Card body End here-->
                               </div><!--Card End here-->
                           </div><!--Col End here-->
                       </div><!--Row End here-->
                       
                       
                   </div> <!--Right side End here-->
                   
               </div><!--Col End here-->
           </div><!--Row End here-->
           
       </div><!--Container End here-->
        
    </section>   <!--   News Main End Here   -->
    
    
  
@endsection