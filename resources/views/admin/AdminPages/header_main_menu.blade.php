@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    
@php
	$menu = DB::table('header_menues')->get();

@endphp


		<div class="tx-right ">
			<a href="" class="btn btn-info pd-x-20" data-toggle="modal" data-target="#header_menu">Add Menu</a>
		</div><!-- pd-y-30 -->


        <div class="sl-page-title">
          <h5>Header Menu</h5>
          <p>Money Exchange Site Menu Add, Remove, Update Here</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Please Click Add Menu to Add New Menu</h6>
        

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">SL NO</th>
                  <th class="wd-15p">Menu Item</th>
                  <th class="wd-15p">Menu Status</th>
                  <th class="wd-20p">Action</th>
                  
                </tr>
              </thead>
              <tbody>

              	@php
              		$sl = 1;
              	@endphp

              	@foreach($menu as $row)
                <tr>

                  <td>{{ $sl++ }}</td>
                  <td>{{ $row->menu_item }}</td>
                  <td>

                    @if($row->status == 1) 
                      <a href="#" class="badge badge-success">Active</a>
                    @else 
                    <a href="#" class="badge badge-danger">Deactive</a>
                    @endif

                  </td>
                  <td>
                  	<a href="{{ URL::to('edit/menu/item/'.$row->id) }}" class="btn btn-info" title="Update Menu?"><i class="far fa-edit"></i></a>
                  	<a href="#" class="btn btn-danger" title="Remove Menu?"><i class="fas fa-trash"></i></a>
                  	
                    <a href="{{ URL::to('menu/status/active/'.$row->id) }}" class="btn btn-success" title="Active Menu?"><i class="far fa-check-square"></i></a>
                    <a href="{{ URL::to('menu/status/deactive/'.$row->id) }}" class="btn btn-danger" title="Deactive Menu?"><i class="far fa-check-square"></i></a>

                  </td>
                  
                </tr>
                @endforeach
              
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->


  <div class="card mg-t-25">  
        <!-- LARGE MODAL -->
        <div id="header_menu" class="modal fade">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
              
              <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add New Menu</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body pd-20">
              
                <form method="post" action="{{ URL::to('add/menu') }}">
                	@csrf
				  <div class="form-group">				   
				    <input type="text" class="form-control" name="menu_item" id="addMenu" aria-describedby="emailHelp" placeholder="Enter New Menu">				   
				  </div>
				 

              </div><!-- modal-body -->

              <div class="modal-footer">
                <button type="submit" class="btn btn-info pd-x-20">Add</button>
                <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
                
              </div>
              </form>
            </div>
          </div><!-- modal-dialog -->
        </div><!-- modal -->
    </div>
</div>


      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection