@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    <div class="tx-right ">
      <a href="" class="btn btn-info pd-x-20" data-toggle="modal" data-target="#logo">Add Logo</a>
    </div><!-- pd-y-30 -->

        <div class="sl-page-title">
          <h5>User Review</h5>
          <p>Money Exchange Site Aproove & Action Here</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
         
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">SL NO</th>
                  <th class="wd-15p">Logo</th>
                  <th class="wd-15p">Tagline</th>
                  <th class="wd-15p">Status</th>
                  <th class="wd-20p">Action</th>
                  
                </tr>
              </thead>
              <tbody>

              	@php
              		$sl = 1;
              	@endphp

              	@foreach($brand as $row)
                <tr>

                  <td>{{ $sl++ }}</td>
                  <td><img src="{{ url($row->brand_logo)}}" alt=""></td>
                  <td>{{ $row->tag_line }}</td>
                  <td>

                    @if($row->status == 1) 
                      <a href="#" class="badge badge-success">Aprooved</a>
                    @else 
                    <a href="#" class="badge badge-warning text-white">Pending</a>
                    @endif

                  </td>
                  <td>
                  	
                  	<a href="{{ URL::to('delete/logo/'.$row->id) }}" class="btn btn-danger" title="Remove?"><i class="fas fa-trash"></i></a>
                  	
                    <a href="{{ URL::to('review/aproove/'.$row->id) }}" class="btn btn-success" title="Aproove?"><i class="far fa-check-square"></i></a>

                    <a href="{{ URL::to('review/pending/'.$row->id) }}" class="btn btn-danger" title="Pending?"><i class="far fa-check-square"></i></a>

                  </td>
                  
                </tr>
                @endforeach
              
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->


        <div class="card mg-t-25">  
        <!-- LARGE MODAL -->
        <div id="logo" class="modal fade">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
              
              <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add New Currency Rate</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body pd-20">
              
                <form method="post" action="{{ URL::to('StoreLogo') }}" enctype="multipart/form-data">
                  @csrf

                  <div class="form-group">           
                    <input type="file" class="form-control" name="brand" id="buy_currency_sign" aria-describedby="emailHelp">
                    <small><i>Note: Select Brand Logo</i></small>
                  </div>


                  <div class="form-group">           
                    <input type="text" class="form-control" name="tagline" id="buy_currency_name" aria-describedby="emailHelp" placeholder="Tagline (Optional)"> 
                    <small><i>Note: This best site in the world</i></small>          
                  </div>
                
       

              </div><!-- modal-body -->

              <div class="modal-footer">
                <button type="submit" class="btn btn-info pd-x-20">Add</button>
                <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
                
              </div>

              </form>
            </div>
          </div><!-- modal-dialog -->
        </div><!-- modal -->
    </div>


</div>


      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection