@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    



		<div class="tx-right ">
			<a href="" class="btn btn-info pd-x-20" data-toggle="modal" data-target="#currencyRate">Add Currency Rate</a>
		</div><!-- pd-y-30 -->


        <div class="sl-page-title">
          <h5>Currency Rate</h5>
          <p>Money Exchange Site Currency rate Add, Remove, Update Here</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Please Click on Add Currency rate to Add New Currency rate</h6>
        

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">SL NO</th>
                  <th class="wd-15p">We Buy Currency</th>
                  <th class="wd-15p">We Sell Currency</th>
                  

                  <th class="wd-15p">Status</th>
                  <th class="wd-20p">Action</th>
                  
                </tr>
              </thead>
              <tbody>

              	@php
              		$sl = 1;
              	@endphp

              @foreach($currencyRate as $row)
                <tr>

                  <td>{{ $sl++ }}</td>
                  <td>
                    <img src="{{ url($row->buy_currency_sign) }}" alt="">
                    {{ $row->we_buy }} {{ $row->buy_currency_name }}
                  </td>

                  <td>
                    <img src="{{ url($row->sell_currency_sign) }}" alt="">
                    {{ $row->we_sell }} {{ $row->sell_currency_name }}
                  </td>

                  <td>
                    @if($row->status == 1)
                      <a href="#" class="badge badge-success">Active</a>
                    @else
                      <a href="#" class="badge badge-danger">Deactive</a>
                    @endif  

                  </td>
                  <td>
                  	<a href="{{ URL::to('edit/currency/rate/'.$row->id) }}" class="btn btn-info" title="Update?"><i class="far fa-edit"></i></a>
                  	<a href="{{ URL::to('delete/currency/rate/'.$row->id) }}" class="btn btn-danger" title="Remove?"><i class="fas fa-trash"></i></a>
                  	
                    <a href="{{ URL::to('currency/rate/active/'.$row->id) }}" class="btn btn-success" title="Active?"><i class="far fa-check-square"></i></a>

                    <a href="{{ URL::to('currency/rate/deactive/'.$row->id) }}" class="btn btn-danger" title="Deactive?"><i class="far fa-check-square"></i></a>

                  </td>
                  
                </tr>
              @endforeach
              
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->


  <div class="card mg-t-25">  
        <!-- LARGE MODAL -->
        <div id="currencyRate" class="modal fade">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
              
              <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add New Currency Rate</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body pd-20">
              
                <form method="post" action="{{ URL::to('add/currency/rate') }}" enctype="multipart/form-data">
                	@csrf

        				  <div class="form-group">				   
        				    <input type="text" class="form-control" name="buy_rate" id="buy_rate" aria-describedby="emailHelp" placeholder="Enter buy rate...">				   
        				  </div>

                  <div class="form-group">           
                    <input type="text" class="form-control" name="buy_currency_name" id="buy_currency_name" aria-describedby="emailHelp" placeholder="Enter buy currency name..."> 
                    <small><i>Ex: TK or USD</i></small>          
                  </div>

                  <div class="form-group">           
                    <input type="file" class="form-control" name="buy_currency_sign" id="buy_currency_sign" aria-describedby="emailHelp">
                    <small><i>Ex: choose currency sign</i></small>
                  </div>

                  <div class="form-group">           
                    <input type="text" class="form-control" name="sell_rate" id="sell_rate" aria-describedby="emailHelp" placeholder="Enter sell rate...">          
                  </div>

                  <div class="form-group">           
                    <input type="text" class="form-control" name="sell_currency_name" id="sell_currency_name" aria-describedby="emailHelp" placeholder="Enter sell currency name...">   
                    <small><i>Ex: TK or USD</i></small>       
                  </div>

                  <div class="form-group">           
                    <input type="file" class="form-control" name="sell_currency_sign" id="sell_currency_sign" aria-describedby="emailHelp">  
                    <small><i>Ex: choose currency sign</i></small>        
                  </div>
			 

              </div><!-- modal-body -->

              <div class="modal-footer">
                <button type="submit" class="btn btn-info pd-x-20">Add</button>
                <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
                
              </div>

              </form>
            </div>
          </div><!-- modal-dialog -->
        </div><!-- modal -->
    </div>
</div>


      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection