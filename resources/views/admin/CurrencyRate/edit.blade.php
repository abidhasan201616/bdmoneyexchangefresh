@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    




        <div class="sl-page-title">
          <h5>Update Currency Rate</h5>
          <p>Money Exchange Site Currency rate Update Here</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
         
        
          <form method="post" action="{{ URL::to('update/currency/rate/'.$currencyData->id) }}" enctype="multipart/form-data">
              @csrf

              <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-3">
                  <h5>Buy Rate</h5>
                </div>
                <div class="col-sm-12 col-md-9 col-lg-9">
                  <div class="form-group">           
                    <input type="text" class="form-control" name="buy_rate" id="buy_rate" aria-describedby="emailHelp" value="{{ $currencyData->we_buy }}">          
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-3">
                  <h5>Buy Currency Name</h5>
                </div>
                <div class="col-sm-12 col-md-9 col-lg-9">
                  <div class="form-group">           
                    <input type="text" class="form-control" name="buy_currency_name" id="buy_currency_name" aria-describedby="emailHelp" value="{{ $currencyData->buy_currency_name }}"> 
                    <small><i>Ex: TK or USD</i></small>          
                  </div>
                </div>
              </div>

              

                  <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                       <h5>Update Sign</h5>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                      <div class="form-group">           
                        <input type="file" class="form-control" name="new_buy_currency_sign" id="buy_currency_sign" aria-describedby="emailHelp">
                        <small><i>Ex: choose currency sign</i></small>
                      </div>
                       <div class="form-group">           
                      <img src="{{ url($currencyData->buy_currency_sign) }}"  name="old_buy_currency_sign" id="old_buy_currency_sign" width="50" height="50" aria-describedby="emailHelp">
                      
                    </div>
                    </div>
                  </div>


                   <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                      <h5>Sell Currency Rate</h5>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                      <div class="form-group">           
                        <input type="text" class="form-control" name="sell_rate" id="sell_rate" aria-describedby="emailHelp" value="{{ $currencyData->we_sell }}">          
                      </div>
                    </div>
                  </div>
                 

                  <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                      <h5>Sell Currency Name</h5>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                      <div class="form-group">           
                        <input type="text" class="form-control" name="sell_currency_name" id="sell_currency_name" aria-describedby="emailHelp" value="{{ $currencyData->sell_currency_name }}">   
                        <small><i>Ex: TK or USD</i></small>       
                      </div>
                    </div>
                  </div>

                  

                  <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                      <h5>Update Sign</h5>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                       <div class="form-group">           
                        <img src="{{ url($currencyData->sell_currency_sign) }}" name="old_sell_currency_sign" id="old_sell_currency_sign" width="50" height="50" aria-describedby="emailHelp">  
                      </div>   

                      <div class="form-group">           
                        <input type="file" class="form-control" name="new_sell_currency_sign" id="sell_currency_sign" aria-describedby="emailHelp">  
                        <small><i>Ex: choose currency sign</i></small>        
                      </div>
                    </div>
                  </div>

                  

                        
     

          </div><!-- modal-body -->

          <div class="modal-footer">
            <button type="submit" class="btn btn-info pd-x-20">Update</button>
            <a href="{{ URL::to('currency/rate') }}" class="btn btn-secondary pd-x-20">Back</a>
            
          </div>

          </form>


        </div>

      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection