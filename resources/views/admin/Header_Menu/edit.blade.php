@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->

  <div class="card mg-t-25 p-4">  
         <h6 class="card-body-title">Please Click Add Menu to Add New Menu</h6>
    <form method="post" action="{{ URL::to('update/menu/item/'.$menu->id) }}">
      @csrf
      <div class="form-group">           
        <input type="text" class="form-control" name="menu_item" value="{{ $menu->menu_item }}">           
      </div>
     
        <button type="submit" class="btn btn-info pd-x-20">Update</button>
    </form>
  </div>


      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection