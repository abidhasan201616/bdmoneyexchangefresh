@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    <div class="tx-right ">
      <a href="" class="btn btn-info pd-x-20" data-toggle="modal" data-target="#status">Add Status</a>
    </div><!-- pd-y-30 -->


        <div class="sl-page-title">
          <h5>Payment Status</h5>
          <p>Note: Pending, Processing, Cancelled, Success status number must be 1,2,3,4</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
         
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">SL NO</th>
                  <th class="wd-15p">Status Name</th>
                  <th class="wd-15p">Status</th>                  
                  <th class="wd-20p">Action</th>
                  
                </tr>
              </thead>
              <tbody>

              	@php
              		$sl = 1;
              	@endphp

              	@foreach($PaymentStatus as $row)
                <tr>

                  <td>{{ $sl++ }}</td>
                  <td>{{ $row->status_name }}</td>
                  
                  <td>

                    @if($row->status == 1) 
                      <a href="#" class="badge badge-warning">Pending</a>
                    @elseif($row->status == 2)
                      <a href="#" class="badge badge-primary text-white">Processing</a>
                    @elseif($row->status == 3)
                      <a href="#" class="badge badge-danger text-white">Cancelled</a>
                      @elseif($row->status == 4)
                      <a href="#" class="badge badge-success text-white">Success</a>
                    @else
                      <a href="#" class="badge badge-danger text-white">Wrong Status Number</a>
                    @endif

                  </td>
                  <td>                  	                 	
                  	
                    <a href="{{ URL::to('edit/payment/status/'.$row->id) }}" class="btn btn-success" title="update?"><i class="far fa-edit"></i></a>

                    <a href="{{ URL::to('delete/status/'.$row->id) }}" class="btn btn-danger" title="Remove?"><i class="fas fa-trash"></i></a>                 

                  </td>
                  
                </tr>
                @endforeach
              
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->


          <div class="card mg-t-25">  
        <!-- LARGE MODAL -->
        <div id="status" class="modal fade">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
              
              <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add New Status</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body pd-20">
              
                <form method="post" action="{{ URL::to('add/payment/status') }}">
                  @csrf

                  <div class="form-group">           
                    <input type="text" class="form-control" name="status_name" id="status_name" placeholder="Status Name...">           
                  </div>

                  <div class="form-group">           
                    <input type="text" class="form-control" name="status_number" id="status_number" placeholder="Status Number...">           
                  </div>        

              </div><!-- modal-body -->

              <div class="modal-footer">
                <button type="submit" class="btn btn-info pd-x-20">Add</button>
                <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
                
              </div>

              </form>
            </div>
          </div><!-- modal-dialog -->
        </div><!-- modal -->
    </div>


</div>


      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection