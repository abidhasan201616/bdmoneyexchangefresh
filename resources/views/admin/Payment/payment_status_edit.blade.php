@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    


        <div class="sl-page-title">
          <h5>Update Payment Status</h5>
          <p>Money Exchange Site Payment Status Update Here</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
         
        
          <form method="post" action="{{ URL::to('update/payment/status/'.$PaymentStatus->id) }}" enctype="multipart/form-data">
              @csrf
              <div class="form-group">           
                <input type="text" class="form-control" name="status_name" id="status_name" value="{{ $PaymentStatus->status_name }}">           
              </div>

              <div class="form-group">           
                <input type="text" class="form-control" name="status_number" id="status_number" value="{{ $PaymentStatus->status }}">           
              </div>


          </div><!-- modal-body -->

          <div class="modal-footer">
            <button type="submit" class="btn btn-info pd-x-20">Update</button>
            <a href="{{ route('payment.status') }}" class="btn btn-secondary pd-x-20">Back</a>
            
          </div>

          </form>


        </div>

      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection