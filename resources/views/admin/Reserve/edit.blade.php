@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    




        <div class="sl-page-title">
          <h5>Update Reserve Amount</h5>
          <p>Money Exchange Site Reserve Amount Add, Remove, Update Here</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
         
        
          <form method="post" action="{{ URL::to('update/reserve/amount/'.$reserve->id) }}" enctype="multipart/form-data">
              @csrf
              <div class="form-group">           
                <input type="text" class="form-control" name="method" id="method" aria-describedby="emailHelp" value="{{ $reserve->method_name }}">           
              </div>

              <div class="form-group">           
                <input type="text" class="form-control" name="reserve_amount" id="reserve" aria-describedby="emailHelp" value="{{ $reserve->reserve_amount }}">           
              </div>

              <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                  <div class="form-group">           
                    <input type="file" class="form-control" name="new_currency_sign" id="currency_sign">           
                  </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                  <img src="{{url($reserve->logo)}}" class="mt-2" name="old_currency_sign" alt="Currency Logo">
                </div>
              </div>              
     

          </div><!-- modal-body -->

          <div class="modal-footer">
            <button type="submit" class="btn btn-info pd-x-20">Update</button>
            <a href="{{ URL::to('reserve/amount') }}" class="btn btn-secondary pd-x-20">Back</a>
            
          </div>

          </form>


        </div>

      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection