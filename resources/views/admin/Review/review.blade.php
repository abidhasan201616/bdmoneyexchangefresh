@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    
        <div class="sl-page-title">
          <h5>User Review</h5>
          <p>Money Exchange Site Aproove & Action Here</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
         
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">SL NO</th>
                  <th class="wd-15p">Name</th>
                  <th class="wd-15p">Review</th>
                  <th class="wd-15p">Status</th>
                  <th class="wd-20p">Action</th>
                  
                </tr>
              </thead>
              <tbody>

              	@php
              		$sl = 1;
              	@endphp

              	@foreach($review as $row)
                <tr>

                  <td>{{ $sl++ }}</td>
                  <td>{{ $row->name }}</td>
                  <td>{{ $row->review }}</td>
                  <td>

                    @if($row->status == 1) 
                      <a href="#" class="badge badge-success">Aprooved</a>
                    @else 
                    <a href="#" class="badge badge-warning text-white">Pending</a>
                    @endif

                  </td>
                  <td>
                  	
                  	<a href="{{ URL::to('delete/review/'.$row->id) }}" class="btn btn-danger" title="Remove?"><i class="fas fa-trash"></i></a>
                  	
                    <a href="{{ URL::to('review/aproove/'.$row->id) }}" class="btn btn-success" title="Aproove?"><i class="far fa-check-square"></i></a>

                    <a href="{{ URL::to('review/pending/'.$row->id) }}" class="btn btn-danger" title="Pending?"><i class="far fa-check-square"></i></a>

                  </td>
                  
                </tr>
                @endforeach
              
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->


</div>


      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection