@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    


        <div class="sl-page-title">
          <h5>Update Send Method</h5>
          <p>Money Exchange Send Method Update Here</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
         
        
          <form method="post" action="{{ URL::to('update/send/method/'.$sendMethodInfo->id) }}" enctype="multipart/form-data">
              @csrf
              <div class="form-group">           
                <input type="text" class="form-control" name="send_method_name" id="send_method_name" value="{{ $sendMethodInfo->send_method_name }}">           
              </div>


          </div><!-- modal-body -->

          <div class="modal-footer">
            <button type="submit" class="btn btn-info pd-x-20">Update</button>
            <a href="{{ route('send.receive') }}" class="btn btn-secondary pd-x-20">Back</a>
            
          </div>

          </form>


        </div>

      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection