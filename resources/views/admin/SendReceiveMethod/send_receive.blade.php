@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->
    <div class="tx-right ">
      <a href="" class="btn btn-info pd-x-20" data-toggle="modal" data-target="#sendMethod">Add Send Method</a>
      <a href="" class="btn btn-info pd-x-20" data-toggle="modal" data-target="#receiveMethod">Add Receive Method</a>
    </div><!-- pd-y-30 -->


        <div class="sl-page-title">
          <h5>Payment Status</h5>
          <p>Send/Receive Method add, delete, update</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
          <h5>Send Method List</h5>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">SL NO</th>
                  <th class="wd-15p">Send Method Name</th>
                  <th class="wd-15p">Status</th>                  
                  <th class="wd-20p">Action</th>
                  
                </tr>
              </thead>
              <tbody>

              	@php
              		$sl = 1;
              	@endphp

              	@foreach($sendMethodInfo as $row)
                <tr>

                  <td>{{ $sl++ }}</td>
                  <td>{{ $row->send_method_name }}</td>
                  
                  <td>

                    @if($row->status == 1) 
                      <a href="#" class="badge badge-success">Active</a>                    
                    @else
                      <a href="#" class="badge badge-warning text-white">Deactive</a>
                    @endif

                  </td>
                  <td>                  	                 	
                  	
                    <a href="{{ URL::to('edit/send/method/'.$row->id) }}" class="btn btn-success" title="update?"><i class="far fa-edit"></i></a>

                    <a href="{{ URL::to('delete/send/method/'.$row->id) }}" class="btn btn-danger" title="Remove?"><i class="fas fa-trash"></i></a>  

                     <a href="{{ URL::to('active/send/method/'.$row->id) }}" class="btn btn-primary" title="Active?"><i class="fas fa-check-square"></i></a> 

                    <a href="{{ URL::to('deactive/send/method/'.$row->id) }}" class="btn btn-warning" title="Deactive?"><i class="fas fa-times"></i></a>               

                  </td>
                  
                </tr>
                @endforeach
              
              </tbody>
            </table>
          </div><!-- table-wrapper -->

          <h5>Receive Method List</h5>
           <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">SL NO</th>
                  <th class="wd-15p">Receive Method Name</th>
                  <th class="wd-15p">Status</th>                  
                  <th class="wd-20p">Action</th>
                  
                </tr>
              </thead>
              <tbody>

                @php
                  $sl = 1;
                @endphp

                @foreach($receiveMethodInfo as $row)
                <tr>

                  <td>{{ $sl++ }}</td>
                  <td>{{ $row->receive_method_name }}</td>
                  
                  <td>

                    @if($row->status == 1) 
                      <a href="#" class="badge badge-success">Active</a>                    
                    @else
                      <a href="#" class="badge badge-warning text-white">Deactive</a>
                    @endif

                  </td>
                  <td>                                      
                    
                    <a href="{{ URL::to('edit/receive/method/'.$row->id) }}" class="btn btn-success" title="update?"><i class="far fa-edit"></i></a>

                    <a href="{{ URL::to('delete/receive/method/'.$row->id) }}" class="btn btn-danger" title="Remove?"><i class="fas fa-trash"></i></a>

                    <a href="{{ URL::to('active/receive/method/'.$row->id) }}" class="btn btn-primary" title="Active?"><i class="fas fa-check-square"></i></a> 

                    <a href="{{ URL::to('deactive/receive/method/'.$row->id) }}" class="btn btn-warning" title="Deactive?"><i class="fas fa-times"></i></a>                  

                  </td>
                  
                </tr>
                @endforeach
              
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->


        <div class="card mg-t-25">  
        <!-- LARGE MODAL -->
        <div id="sendMethod" class="modal fade">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
              
              <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Send Method</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body pd-20">
              
                <form method="post" action="{{ URL::to('add/send/method') }}">
                  @csrf

                  <div class="form-group">           
                    <input type="text" class="form-control" name="send_method_name" id="send_method_name" placeholder="Send Method Name...">           
                  </div>

                  
              </div><!-- modal-body -->

              <div class="modal-footer">
                <button type="submit" class="btn btn-info pd-x-20">Add</button>
                <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
                
              </div>

              </form>
            </div>
          </div><!-- modal-dialog -->
        </div><!-- modal -->
    </div>

     <div class="card mg-t-25">  
        <!-- LARGE MODAL -->
        <div id="receiveMethod" class="modal fade">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
              
              <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Receive Method</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body pd-20">
              
                <form method="post" action="{{ URL::to('add/receive/method') }}">
                  @csrf

                  <div class="form-group">           
                    <input type="text" class="form-control" name="receive_method_name" id="receive_method_name" placeholder="Receive Method Name...">           
                  </div>

                  
              </div><!-- modal-body -->

              <div class="modal-footer">
                <button type="submit" class="btn btn-info pd-x-20">Add</button>
                <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
                
              </div>

              </form>
            </div>
          </div><!-- modal-dialog -->
        </div><!-- modal -->
    </div>


</div>


      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection