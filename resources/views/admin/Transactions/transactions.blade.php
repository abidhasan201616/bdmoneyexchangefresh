@extends('admin.admin_master')

@section('admin_content')

 <!-- ########## START: MAIN PANEL ########## -->


        <div class="sl-page-title">
          <h5>Currency Rate</h5>
          <p>Money Exchange Site Currency rate Add, Remove, Update Here</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Please Click on Add Currency rate to Add New Currency rate</h6>
        

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">SL NO</th>
                  <th class="wd-15p">Order ID</th>
                  <th class="wd-15p">Name</th>                
                  <th class="wd-15p">Email</th>                
                  <th class="wd-15p">Phone</th>                
                  <th class="wd-15p">Address 1</th>                
                  <th class="wd-15p">Address 2</th>                
                  <th class="wd-15p">Sender Account</th>                
                  <th class="wd-15p">Txd ID</th>                
                  <th class="wd-15p">Receiver Account</th>                
                  <th class="wd-15p">Send Method</th>                
                  <th class="wd-15p">Receive Method</th>                
                  <th class="wd-15p">Send Amount</th>                
                  <th class="wd-15p">Receive Amount</th>                
                  <th class="wd-15p">Status</th>
                  <th class="wd-20p">Action</th>
                  
                </tr>
              </thead>
              <tbody>

              	@php
              		$sl = 1;
              	@endphp

              @foreach($transactions as $row)
                <tr>

                  <td>{{ $sl++ }}</td>

                  <td> {{ $row->order_id }} </td>
                  <td> {{ $row->name }} </td>
                  <td> {{ $row->email }} </td>
                  <td> {{ $row->phone }} </td>
                  <td> {{ $row->address_one }} </td>
                  <td> {{ $row->address_two }} </td>
                  <td> {{ $row->sender_account }} </td>
                  <td> {{ $row->txdId }} </td>
                  <td> {{ $row->receiver_account }} </td>
                  <td> {{ $row->send_method }} </td>
                  <td> {{ $row->receive_method }} </td>
                  <td> {{ $row->send_amount }} </td>
                  <td> {{ $row->receive_amount }} </td>

                 


                  <td>
                    @if($row->status == 1)
                      <a href="#" class="badge badge-warning text-white">Pending</a>
                    @elseif($row->status == 2)
                      <a href="#" class="badge badge-primary">Processing</a>
                    @elseif($row->status == 3)
                      <a href="#" class="badge badge-danger">Cancelled</a>
                    @elseif($row->status == 4)
                      <a href="#" class="badge badge-success">Success</a>

                    @else
                      
                    @endif  

                  </td>
                  <td>
                    <form action="{{ URL::to('update/transactions/'.$row->id) }}" method="post">
                      
                      @csrf

                      <select name="transaction_status">

                        <option value="">--Select One--</option>
                        <option value="pending">Pending</option>
                        <option value="processing">Processing</option>
                        <option value="cancelled">Cancelled</option>
                        <option value="success">Success</option>

                      </select>

                      <input type="submit" class="btn btn-sm btn-primary" value="Submit">
                  	</form>
                  

                  </td>
                  
                </tr>
              @endforeach
              
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->



</div>


      
    <!-- ########## END: MAIN PANEL ########## -->

@endsection