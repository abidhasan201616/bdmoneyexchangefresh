	 <link href="{{asset('public/assets/AdminBackend/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
	 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link href="{{asset('public/assets/AdminBackend/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/AdminBackend/lib/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/AdminBackend/lib/rickshaw/rickshaw.min.css')}}" rel="stylesheet">

    <link href="{{asset('public/assets/AdminBackend/lib/highlightjs/github.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/AdminBackend/lib/datatables/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/AdminBackend/lib/select2/css/select2.min.css')}}" rel="stylesheet">


    <!-- Starlight CSS -->
    <link rel="stylesheet" href="{{asset('public/assets/AdminBackend/css/starlight.css')}}">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">