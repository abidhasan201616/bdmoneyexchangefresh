<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{{ asset('public/assets/UserFrontend/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/UserFrontend/css/bootstrap.min.css')}}">
    {{-- <link rel="shortcut icon" href="{{ asset('public/assets/UserFrontend/img/bkash.jpg')}}"> --}}
    <link rel="stylesheet" href="{{ asset('public/assets/UserFrontend/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/UserFrontend/css/owl.carousel.css')}}">

    <title>BD Money Exchange</title>
  </head>
  <body>
   <!--   Header Start Here   -->
   <header class="header p-2">
       <div class="container">
           <div class="row justify-content-between">
                <div class="col-4 ">                             
                   <div class="card-body">        
                        
                    </div> 
                </div>
                <div class="col-4 ">
                             
                   <div class="card-body text-right">        
                        <button type="button" class="btn btn-light pl-4 pr-3" id="login">Login</button>
                        <button type="button" class="btn btn-light">Register</button>
                    </div> 
                </div>
            </div>
               
           </div>
      
       
      
   </header>
   <!--   Header End Here   -->
      
<!--   Navbar Start Here   -->
<nav class="navbar navbar-expand-lg bg-color-custom">
    <a class="navbar-brand" href="#">
        <img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="" alt="">
    </a>
  <button class="navbar-toggler border" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<!--    <span class="navbar-toggler-icon btn btn-success"></span>-->
 <span class="text-warning"><b>Menu</b></span>
  </button>

  <div class="collapse navbar-collapse " id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto  text-uppercase font-weight-bold">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="#">Rate</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="#">Testimonials</a>
      </li>  
      
      <li class="nav-item">
        <a class="nav-link" href="#">Contact Us</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="#">Help</a>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item text-warning" href="#">Action</a>
          <a class="dropdown-item text-warning" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item text-warning" href="#">Something else here</a>
        </div>
      </li>
     
        
    </ul>
   
  </div>
</nav>
<!--   Navbar End Here   -->

    
    <!--   News Start Here   -->
    <div class="section">
        <div class="row">
           
                <div class="col-sm-12 col-md-12 col-lg-12">
                   
                    <ul class="list-group">
                            
                          <li class="list-group-item">
                              <a href="#" > 
                                  <marquee behavior="" direction=""><h5 class="d-inline">NEWS UPDATE : </h5>This is news!</marquee>
                              </a>
                          </li>
                      </ul>
                    
                </div>
            
        </div>
    </div>
    
    <!--   News End Here   -->
    
    
    <!--   Main Start Here   -->
    <section class="main_section">
       <div class="container">
           <div class="row">
               <div class="col-sm-8 col-md-8 col-lg-8">
                   
                   <!--Main Left Going here-->
                    <div class="main_left_side mt-2">
                       <!--Card Going here-->
                       <div class="card">
                           <div class="card-body">
                               <h2 class="text-center text-success"><i class="fab fa-gg-circle"></i> Exchange</h2>
                               <hr>
                              
                              <div class="row">
                                  <div class="col-sm-5 col-md-5 col-lg-5">
                                      <div class="send_method">
                                          <select class="input-group p-2">
                                             <option class="input-group-item p-2">Select Method</option>
                                              <option class="input-group-item p-2">
                                              <img src="img/bkash.jpg" alt="fd">
                                              Bkash
                                              
                                              </option>
                                          </select>
                                      </div>
                                  </div>
                                  
                                  <div class="col-sm-2 col-md-2 col-lg-2">
                                      <div class="exchange m-auto text-success text-center pt-1">
                                          <i class="fas fa-arrows-alt-h "></i>
                                      </div>
                                  </div>
                                  
                                  
                                  <div class="col-sm-5 col-md-5 col-lg-5">
                                      <div class="receive_method">
                                          <select class="input-group p-2">
                                             <option class="input-group-item p-2">Select Method</option>
                                              <option class="input-group-item p-2">
                                              <img src="img/bkash.jpg" alt="fd">
                                              Bkash
                                              
                                              </option>
                                          </select>
                                      </div>
                                  </div>                                  
                                                                
                              </div><!--Row End here-->
                              
                              <hr>
                              <div class="container">
                                  <div class="row">
                                      <div class="col-md-6 offset-md-3">
                                           <div class="exchange_btn text-center">
                                              <button type="button" class="btn btn-light"><h4>  Next <i class="fas fa-chevron-right"></i> </h4></button>
                                          </div>
                                      </div><!--Col-md-6 End here-->
                                  </div><!--Row End here-->
                              </div><!--Container End here-->
                             
                               
                              
                           </div><!--Card Body End here-->
                           
                       </div><!--Card End here-->
                  
                        <div class="row mt-2">
                           <div class="col-sm-12 col-md-12 col-lg-12">

                              <div class="card">
                                  <div class="card-body">
                                      <div class="from_admin">
                                           <h5>আসসালামু আলাইকুম, OurexBD সকল ইউজার কে শুভেচ্ছা ও স্বাগতম। আমাদের OurexBD সাইট আপডেট করা হয়েছে আপনাদের সুভিধার জন্য। OurexBD সকল ইউজারদেরকে Dollar Buy Sell করার আগে নতুন করে একটা ACCOUNT করে নিতে হবে। সততার সাথে কাজ করে চলছে OurexBD। যে কনো প্রয়োজন এ আমাদের Support Number কল করুন। আমাদের ওয়েবসাইট ছারা অন্ন কনো মাধ্যমে লেনদেন করিনা। ধন্যবাদ আমাদের সাথে থাকার জন্য।</h5>
                                       </div><!--From admin End here-->
                                  </div><!--Card Body End here-->
                              </div><!--Card End here-->


                           </div>
                       </div>
                      
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="card mt-2">
                                   <div class="card-body">
                                      <div class="exchange_head text-center">
                                          <h2 class="text-success"> <i class="far fa-clock"></i> Latest Exchange</h2>
                                      </div>
                                      
                                      <div class="latest_exchange">
                                          <table class="table table-responsive table-hover">
                                              <thead class="thead-light">
                                                <tr>
                                                  <th scope="col">SL</th>     
                                                  <th scope="col">Send</th>
                                                  <th scope="col">Received</th>
                                                  <th scope="col">User</th>
                                                  <th scope="col">Date And Time</th>
                                                  <th scope="col">Status</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                               
                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-success">Success</span>
                                                    
                                                  </td>
                                                </tr>
                                                
                                                 <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>                                                  
                                                    <span class="badge badge-danger">Cancelled</span>
                                                  </td>
                                                </tr>
                                                
                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-primary">Proccessing</span>
                                                   
                                                  </td>
                                                </tr>
                                                
                                                 <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>
                                                
                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>


                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>

                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>


                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>


                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash<br>TK. 98.00</td>
                                                  <td><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash <br> USD 1.00</td>
                                                  <td>Al-amin</td>
                                                  <td>28/08/2020 8:30pm</td>
                                                  <td>
                                                    
                                                    <span class="badge badge-warning">Waiting for payment</span>
                                                   
                                                  </td>
                                                </tr>
                                                
                                              
                                         
                                           
                                                
                                                
                                                
                                              </tbody>
                                            </table>
                                      </div>
                                      
                                       
                                   </div>
                               </div>
                               
                               
                               
                           </div>
                       </div>
                       
                       
                    </div> <!--Main Left End-->
                    
                   
               </div>
               
               <div class="col-sm-4 col-md-4 col-lg-4">
                   <div class="main_right_side mt-2">
                       <div class="card">
                           <div class="card-body">
                               <h4 class="text-center text-success"> <i class="fas fa-coins"></i> Our Reserved</h4>
                               <hr>
                               
                               <table class="table text-center  w-100">
                                  <thead class="thead-dark">
                                    <tr>
                                      <th scope="col">Method</th>
                                      <th scope="col">Amount</th>                                      
                                    </tr>
                                  </thead>
                                  
                                  <tbody>
                                   
                                    <tr>
                                      <th scope="row"><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash</th>
                                      <td>TK 888</td>
                                     
                                    </tr>
                                    
                                    <tr>
                                      <th scope="row"><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash</th>
                                      <td>TK 888</td>
                                     
                                    </tr>
                                    
                                    <tr>
                                      <th scope="row"><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash</th>
                                      <td>TK 888</td>
                                     
                                    </tr>
                                    
                                    <tr>
                                      <th scope="row"><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash</th>
                                      <td>TK 888</td>
                                     
                                    </tr>
                                      
                                      
                                      <tr>
                                      <th scope="row"><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash</th>
                                      <td>TK 888</td>
                                     
                                    </tr>
                                    
                                    
                                   
                                  </tbody>
                                  
                                </table>
                           </div>
                       </div>
                       
                    <!-- Rate Table going here-->
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="card mt-2">
                                   <div class="card-body">
                                       <div class="rate text-center text-success">
                                           <h4> <i class="fas fa-signal"></i> Exchange Rate</h4>
                                       </div>
                                       
                                       <div class="rate_table">
                                           
                                           <table class="table text-center">
                                              <thead class="thead-dark">
                                                <tr>
                                                  <th scope="col">USD</th>
                                                  <th scope="col">We Buy</th>                                   
                                                  <th scope="col">We Sell</th>                                      
                                                </tr>
                                              </thead>

                                              <tbody>

                                                <tr>
                                                  <th scope="row"><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash</th>
                                                  <td>TK 90</td>
                                                  <td>TK 98</td>

                                                </tr>
                                                
                                                <tr>
                                                  <th scope="row"><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash</th>
                                                  <td>TK 90</td>
                                                  <td>TK 98</td>

                                                </tr>
                                                
                                                <tr>
                                                  <th scope="row"><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash</th>
                                                  <td>TK 90</td>
                                                  <td>TK 98</td>

                                                </tr>
                                                
                                                <tr>
                                                  <th scope="row"><img src="{{asset('public/assets/UserFrontend/img/bkash.jpg')}}" width="30" height="30" alt="">Bkash</th>
                                                  <td>TK 90</td>
                                                  <td>TK 98</td>

                                                </tr>
                                                
                                               </tbody>
                                      
                                           </table>
                                       </div><!--Rate table End here-->
                                       
                                       
                                   </div><!--Card body End here-->
                               </div><!--Card End here-->
                           </div><!--Col End here-->
                       </div><!--Row End here-->
                       
                       <div class="row">
                           <div class="col-sm-12 col-md-12 col-lg-12">
                               <div class="card mt-2">
                                   <div class="card-body">
                                       <div class="rate text-center text-success">
                                           <h4> <i class="fas fa-smile-beam"></i> Our Review</h4>
                                       </div>
                                       
                                       <div class="review">
                                           <div class="parent-box owl-carousel">
                                                
                                                   <div class="item">
                                                        <div class="card">

                                                          <div class="card-body">
                                                            <h5 class="card-title"><img src="{{asset('public/assets/UserFrontend/img/avatar.jpg')}}" width="30" height="150" alt="">Here Name</h5>
                                                            <p class="card-text">
                                                            Some quick example text to build on the card title and make up the bulk of the card's content.</p>

                                                          </div>
                                                        </div>

                                                    </div>
                                                    
                                                     <div class="item">
                                                        <div class="card">

                                                          <div class="card-body">
                                                            <h5 class="card-title"><img src="{{asset('public/assets/UserFrontend/img/avatar.jpg')}}" width="30" height="150" alt="">Here Name</h5>
                                                            <p class="card-text">
                                                            Some quick example text to build on the card title and make up the bulk of the card's content.</p>

                                                          </div>
                                                        </div>

                                                    </div>
                                                    
                                                     <div class="item">
                                                        <div class="card">

                                                          <div class="card-body">
                                                            <h5 class="card-title"><img src="{{asset('public/assets/UserFrontend/img/avatar.jpg')}}" width="30" height="150" alt="">Here Name</h5>
                                                            <p class="card-text">
                                                            Some quick example text to build on the card title and make up the bulk of the card's content.</p>

                                                          </div>
                                                        </div>

                                                    </div>
                                              

                                            </div>
                                           
                                           
                                       </div><!--Review End here-->
                                       
                                       
                                   </div><!--Card body End here-->
                               </div><!--Card End here-->
                           </div><!--Col End here-->
                       </div><!--Row End here-->
                       
                       
                   </div> <!--Right side End here-->
                   
               </div><!--Col End here-->
           </div><!--Row End here-->
           
       </div><!--Container End here-->
        
    </section>   <!--   News Main End Here   -->
    
    
<section class="get_in_touch mt-2">
    <div class="container">
        <div class="row text-center">
          
          <div class="col-sm-6 col-md-6 col-lg-6 offset-md-3  col justify-content-md-center">
           <div class="get_in_touch_text m-auto">
               <h1>Review</h1>
               <p>Share your experience with <a href="#" class="text-warning">bdMoneyExchange</a></p>
           </div>
           
           <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="get_in_touch_form">
                <form action="">
                    <input type="text" name="" class="form-control" placeholder="Your Full name...">
                    
                    <br/>
                    <br/>
                    <textarea name="" class="form-control" rows="5" cols="8" placeholder="Share your experience with us" ></textarea>
                    
                </form>               
                
            </div> <!--!get_in_touch_form-->
            
              </div>
            <br/>          
            
            
            <div class="button_send_message text-center">
                <input type="submit" name="" value="SEND REVIEW">
            </div>
            </div>
        </div>
    </div>
    
    <br/>
    <br/>
    
    <section class="footer footer_text">
        <div class="container">
            <div class="row">
              
               <div class="col-sm-12 col-md-3 col-lg-3">
                   <div class="for_you">
                       <h2>Here for you</h2>
                       <p>Call us 24/7. We are there for your support.</p>
                   </div>
               </div>
               
               
                <div class="col-sm-12 col-md-3 col-lg-3">
                   <div class="about_neve">
                       <h2>About Neve</h2>
                       <p>Call us 24/7. We are there for your support.</p>
                   </div>
               </div>
               
               
               <div class="col-sm-12 col-md-3 col-lg-3">
                   <div class="find_us">
                       <h2>Where find us?</h2>
                       <p>Call us 24/7. We are there for your support.</p>
                   </div>
               </div>
               
               
               <div class="col-sm-12 col-md-3 col-lg-3">
                   <div class="keep_touch">
                       <h2>Keep in touch</h2>
                       
                       <div class="icon">
                          <a href="#"><i class="fab fa-facebook-square"></i></a>
                           
                           <a href=""><i class="fab fa-twitter-square"></i></a>
                           <a href=""> <i class="fab fa-linkedin"></i></a>
                           
                           <a href=""><i class="fab fa-youtube"></i></a>



                       </div>
                       
                   </div>
               </div>
              
            </div> <!-- Row End -->
        </div>
    </section>
    
</section><!--get_in_touch end-->  
    
     
     
      
      
      

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    
    <script src="{{asset('public/assets/UserFrontend/js/bootstrap.js')}}"></script>
    <script src="{{asset('public/assets/UserFrontend/js/bootstrap.min.js')}}"></script>
    
    <script src="{{asset('public/assets/UserFrontend/js/jquery.js')}}"></script>
	<script src="{{asset('public/assets/UserFrontend/js/owl.carousel.js')}}"></script>
    <script>
          $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            autoplay:true,
            responsive:{
                0:{
                    items:1
                }
            }
        })
      </script>
  </body>
</html>