<section class="get_in_touch mt-2">
    <div class="container">
        <div class="row text-center">
          
          <div class="col-sm-6 col-md-6 col-lg-6 offset-md-3  col justify-content-md-center">
           <div class="get_in_touch_text m-auto">
               <h1>Review</h1>
               <p>Share your experience with <a href="#" class="text-warning">bdMoneyExchange</a></p>
           </div>
           
           <div class="col-sm-12 col-md-12 col-lg-12">

            <div class="get_in_touch_form">

                <form action="{{ URL::to('StoreReview') }}" method="post">
                  @csrf
                    <input type="text" name="name" class="form-control" placeholder="Your Full name...">
                    
                    <br/>
                    <br/>
                    <textarea name="review" class="form-control" rows="5" cols="8" placeholder="Share your experience with us" ></textarea>
                    
                             
                
            </div> <!--!get_in_touch_form-->
            
              </div>
            <br/>          
            
            
            <div class="button_send_message text-center">
                <input type="submit" name="" value="SEND REVIEW">
            </div>
            </form>  
            </div>
        </div>
    </div>
    
    <br/>
    <br/>
    
    <section class="footer footer_text">
        <div class="container">
            <div class="row">
              
               <div class="col-sm-12 col-md-3 col-lg-3">
                   <div class="for_you">
                       <h2>Here for you</h2>
                       <p>Call us 24/7. We are there for your support.</p>
                   </div>
               </div>
               
               
                <div class="col-sm-12 col-md-3 col-lg-3">
                   <div class="about_neve">
                       <h2>About Neve</h2>
                       <p>Call us 24/7. We are there for your support.</p>
                   </div>
               </div>
               
               
               <div class="col-sm-12 col-md-3 col-lg-3">
                   <div class="find_us">
                       <h2>Where find us?</h2>
                       <p>Call us 24/7. We are there for your support.</p>
                   </div>
               </div>
               
               
               <div class="col-sm-12 col-md-3 col-lg-3">
                   <div class="keep_touch">
                       <h2>Keep in touch</h2>
                       
                       <div class="icon">
                          <a href="#"><i class="fab fa-facebook-square"></i></a>
                           
                           <a href=""><i class="fab fa-twitter-square"></i></a>
                           <a href=""> <i class="fab fa-linkedin"></i></a>
                           
                           <a href=""><i class="fab fa-youtube"></i></a>



                       </div>
                       
                   </div>
               </div>
              
            </div> <!-- Row End -->
        </div>
    </section>
    
</section><!--get_in_touch end-->