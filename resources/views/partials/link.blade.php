  <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<link href="{{asset('public/frontend/css/all.min.css')}}" rel="stylesheet" type="text/css">
  		<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
{{-- User Template Going Here --}}
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{{ asset('public/assets/UserFrontend/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/UserFrontend/css/bootstrap.min.css')}}">

    @if($brand)
        <link rel="shortcut icon" href="{{ asset($brand->brand_logo)}}">
    @else
    <link rel="shortcut icon" href="{{ asset('public/assets/AdminBackend/img/logo.png')}}">
    @endif

    <link rel="stylesheet" href="{{ asset('public/assets/UserFrontend/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/UserFrontend/css/owl.carousel.css')}}">

    <link href="{{asset('public/frontend/css/sb-admin-2.min.css')}}" rel="stylesheet">
{{-- User Template End Here --}}

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">