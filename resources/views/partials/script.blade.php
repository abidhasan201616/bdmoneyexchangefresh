 <!-- Bootstrap core JavaScript-->
  {{-- <script src="{{asset('public/frontend/js/jquery.min.js')}}"></script> --}}
  <script src="{{asset('public/frontend/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="p{{asset('public/frontend/js/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{asset('public/frontend/js/sb-admin-2.min.js')}}"></script>

  <!-- User Frontend Template Going Here -->
  
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    
    <script src="{{asset('public/assets/UserFrontend/js/bootstrap.js')}}"></script>
    <script src="{{asset('public/assets/UserFrontend/js/bootstrap.min.js')}}"></script>
    
    <script src="{{asset('public/assets/UserFrontend/js/jquery.js')}}"></script>
	<script src="{{asset('public/assets/UserFrontend/js/owl.carousel.js')}}"></script>
    <script>
          $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            autoplay:true,
            responsive:{
                0:{
                    items:1
                }
            }
        })
      </script>
  <!-- User Frontend Template End Here -->

  {{-- Toaster.js start--}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
  @if(Session::has('messege'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'info':
             toastr.info("{{ Session::get('messege') }}");
             break;
        case 'success':
            toastr.success("{{ Session::get('messege') }}");
            break;
        case 'warning':
           toastr.warning("{{ Session::get('messege') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('messege') }}");
            break;
    }
  @endif
</script> 
{{-- Toaster.js end--}}