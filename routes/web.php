<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin/home', 'HomeController@handleAdmin')->name('admin.route')->middleware('admin');


Route::get('/logout', 'Auth\LoginController@logout');

// Route::get('')->name();

// Login Register Route Going here
Route::get('login','Auth\LoginController@LoginUserForm')->name('login');

// Admin Panel Route going here
//Header Main Menu route going here
Route::get('header/main/menu','Admin\HeaderMainController@HeaderMenu')->name('header.mainmenu');
Route::post('add/menu','Admin\HeaderMainController@AddMenu');
Route::get('menu/status/deactive/{id}','Admin\HeaderMainController@DeactiveStatus');
Route::get('menu/status/active/{id}','Admin\HeaderMainController@ActiveStatus');
Route::get('edit/menu/item/{id}','Admin\HeaderMainController@EditMenuItem');
Route::post('update/menu/item/{id}','Admin\HeaderMainController@UpdateMenuItem');


// Reserved Amount Route going here
Route::get('reserve/amount','Admin\Reserve\ReserveController@ReserveAmount')->name('reserve.amount');
Route::post('add/reserve/amount','Admin\Reserve\ReserveController@AddReserveAmount');
Route::get('edit/reserve/amount/{id}','Admin\Reserve\ReserveController@EditReserveAmount');
Route::post('update/reserve/amount/{id}','Admin\Reserve\ReserveController@UpdateReserveAmount');
Route::get('reserve/amount/active/{id}','Admin\Reserve\ReserveController@ActiveReserveAmount');
Route::get('reserve/amount/deactive/{id}','Admin\Reserve\ReserveController@DeactiveReserveAmount');
Route::get('delete/reserve/amount/{id}','Admin\Reserve\ReserveController@DeleteReserveAmount');

// Currency Rate Route going here
Route::get('currency/rate','Admin\CurrencyRate\CurrencyRateController@ViewCurrencyRate')->name('currency.rate');
Route::post('add/currency/rate','Admin\CurrencyRate\CurrencyRateController@AddCurrencyRate')->name('add.currency.rate');
Route::get('edit/currency/rate/{id}','Admin\CurrencyRate\CurrencyRateController@EditCurrencyRate');
Route::post('update/currency/rate/{id}','Admin\CurrencyRate\CurrencyRateController@UpdateCurrencyRate');
Route::get('currency/rate/deactive/{id}','Admin\CurrencyRate\CurrencyRateController@DeactiveCurrencyRate');
Route::get('currency/rate/active/{id}','Admin\CurrencyRate\CurrencyRateController@ActiveCurrencyRate');
Route::get('delete/currency/rate/{id}','Admin\CurrencyRate\CurrencyRateController@DeleteCurrencyRate');

// Review route going here
Route::get('review/','Admin\Review\ReviewController@review')->name('review');
Route::post('StoreReview/','Admin\Review\ReviewController@StoreReview');
Route::get('review/aproove/{id}','Admin\Review\ReviewController@AprooveReview');
Route::get('review/pending/{id}','Admin\Review\ReviewController@PendingReview');
Route::get('delete/review/{id}','Admin\Review\ReviewController@DeleteReview');

// Review route End  here

// Brand Logo route Going  here
Route::get('brand/logo','Admin\BrandLogo\BrandLogoController@BrandLogo')->name('brand');
Route::post('StoreLogo','Admin\BrandLogo\BrandLogoController@StoreBrandLogo');
Route::get('delete/logo/{id}','Admin\BrandLogo\BrandLogoController@DeleteBrandLogo');
// Brand Logo route End  here


// Profile route Start  here
Route::get('profile/','Admin\Profile\ProfileController@Profile')->name('profile');
Route::post('store/profile/data/','Admin\Profile\ProfileController@StoreProfileData')->name('store.profile.data');
Route::post('update/profile/{id}','Admin\Profile\ProfileController@UpdateProfileData');
Route::post('upload/photo/{id}','Admin\Profile\ProfileController@UpdateProfilePhoto');

// Profile route End  here

// Payment Status route Start  here
Route::get('payment/status','Admin\PaymentStatus\PaymentStatusController@index')->name('payment.status');
Route::post('add/payment/status','Admin\PaymentStatus\PaymentStatusController@StorePaymentStatus');
Route::get('edit/payment/status/{id}','Admin\PaymentStatus\PaymentStatusController@EditPaymentStatus');
Route::post('update/payment/status/{id}','Admin\PaymentStatus\PaymentStatusController@UpdatePaymentStatus');
Route::get('delete/status/{id}','Admin\PaymentStatus\PaymentStatusController@DeletePaymentStatus');


// Payment Status route End  here

// Send Receive Method route Start  here
Route::get('send/receive','Admin\SendReceiveMethod\SendReceiveMethodController@SendReceiveView')->name('send.receive');
Route::post('add/send/method','Admin\SendReceiveMethod\SendReceiveMethodController@AddSendMethod');
Route::get('edit/send/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@EditSendMethod');
Route::post('update/send/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@UpdateSendMethod');
Route::get('delete/send/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@DeleteSendMethod');
Route::post('add/receive/method','Admin\SendReceiveMethod\SendReceiveMethodController@AddReceiveMethod');
Route::get('edit/receive/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@EditReceiveMethod');
Route::post('update/receive/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@UpdateReceiveMethod');
Route::get('delete/receive/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@DeleteReceiveMethod');
Route::get('deactive/receive/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@DeactiveReceiveMethod');
Route::get('active/receive/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@ActiveReceiveMethod');
Route::get('deactive/send/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@DeactiveSendMethod');
Route::get('active/send/method/{id}','Admin\SendReceiveMethod\SendReceiveMethodController@ActiveSendMethod');
// Send Receive Method route End  here

// Transaction or Exchange Method route Start  here
Route::get('select/method','Admin\Transaction\TransactionController@StepOne');
Route::post('store/transaction','Admin\Transaction\TransactionController@StoreTransaction');
Route::get('success/transaction','Admin\Transaction\TransactionController@SuccessTransaction')->name('transaction.success');
Route::get('admin/view/transaction','Admin\Transaction\TransactionController@AdminViewTransaction')->name('admin.view.transaction');
Route::post('update/transactions/{id}','Admin\Transaction\TransactionController@UpdateTransactionStatus');

// Transaction or Exchange Method route End  here